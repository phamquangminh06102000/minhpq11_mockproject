package com.example.minh.data.repository

import app.cash.turbine.test
import com.example.minh.data.mapper.UserMapper
import com.example.minh.data.model.UserFirebase
import com.example.minh.data.network.firebase.user.UserDataSource
import com.example.minh.domain.FireBaseState
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class UserRepositoryImplTest {


    @MockK
    lateinit var userDataSource: UserDataSource

    @MockK
    lateinit var userRepositoryImpl: UserRepositoryImpl

    @Before
    fun setUp() {
        userDataSource = mockk()
        userRepositoryImpl = UserRepositoryImpl(userDataSource)
    }

    @Test
    fun givenEmailAndPassword_whenCallLogin_returnFirebaseSuccess() = runTest {
        val expected = FireBaseState.Success<String>(null)

        coEvery { userDataSource.login("minh", "minh") } returns expected
        val result = userRepositoryImpl.login("minh", "minh")
        Truth.assertThat(result).isEqualTo(expected)
    }

    @Test
    fun whenCallGetCurrentUser_returnInformationUser() = runTest {
        val expectCurrent = UserFirebase()
        val expect = flow {
            emit(expectCurrent)
        }

        coEvery {
            userDataSource.getCurrentUser()
        } returns expect

        userRepositoryImpl.getCurrentUser().test {
            Truth.assertThat(UserMapper.mapFromDomainModel(awaitItem()))
                .isEqualTo(expectCurrent)
            awaitComplete()
        }
    }

    @Test
    fun givenValidInfo_whenCallRegister_returnFirebaseSuccess() = runTest {

        val validInfo = UserFirebase(
            userName = "Minh",
            email = "admin@gmail.com",
            password = "Fsoft123."
        )

        val expect = FireBaseState.Fail<String>("Empty email", "")

        coEvery {
            userDataSource.register(validInfo)
        } returns expect

        val actual = userRepositoryImpl.register(UserMapper.mapFromFirebaseModel(validInfo))

        Truth.assertThat(actual).isEqualTo(expect)
    }

    @Test
    fun givenEmptyEmail_whenCallRegister_returnFirebaseSuccess() = runTest {

        val emptyEmailInfo = UserFirebase(
            userName = "Minh",
            email = "",
            password = "Fsoft123."
        )

        val expect = FireBaseState.Fail<String>("Empty email", "")

        coEvery {
            userDataSource.register(emptyEmailInfo)
        } returns expect

        val actual = userRepositoryImpl.register(UserMapper.mapFromFirebaseModel(emptyEmailInfo))

        Truth.assertThat(actual).isEqualTo(expect)
    }

    @Test
    fun givenEmptyPassword_whenCallRegister_returnFirebaseSuccess() = runTest {

        val emptyEmailInfo = UserFirebase(
            userName = "Minh",
            email = "admin@gmail.com",
            password = ""
        )

        val expect = FireBaseState.Fail<String>("Empty email", "")

        coEvery {
            userDataSource.register(emptyEmailInfo)
        } returns expect

        val actual = userRepositoryImpl.register(UserMapper.mapFromFirebaseModel(emptyEmailInfo))

        Truth.assertThat(actual).isEqualTo(expect)
    }
}