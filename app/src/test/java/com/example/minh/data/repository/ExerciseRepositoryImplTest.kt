package com.example.minh.data.repository

import app.cash.turbine.test
import com.example.minh.data.mapper.ExerciseMapper
import com.example.minh.data.model.ExerciseApi
import com.example.minh.data.network.api.exerciseapi.ExerciseMockApiDataSourceImpl
import com.example.minh.domain.model.Exercise
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class ExerciseRepositoryImplTest {

    private lateinit var exerciseMockApiDatasource: ExerciseMockApiDataSourceImpl
    private lateinit var exerciseMockApiRepository: ExerciseRepositoryImpl

    @Before
    fun setUp() {
        exerciseMockApiDatasource = mockk()
        exerciseMockApiRepository = ExerciseRepositoryImpl(exerciseMockApiDatasource)
    }

    @Test
    fun whenCallGetExercises_returnListExercise() = runTest {
        val expectList = listOf<ExerciseApi>()
        val expect = flow<List<ExerciseApi>> {
            emit(expectList)
        }

        coEvery {
            exerciseMockApiDatasource.getExercises()
        } returns expect

        exerciseMockApiRepository.getExercises().test {
            Truth.assertThat(ExerciseMapper.mapFromDomainModel(awaitItem()))
                .isEqualTo(expectList)
            awaitComplete()
        }


    }
}