package com.example.minh.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.example.minh.databinding.ListExerciseImageItemBinding
import com.example.minh.domain.model.ExerciseImage
import com.example.minh.presentation.base.BaseAdapter

class ExerciseImageAdapter : BaseAdapter<ListExerciseImageItemBinding, ExerciseImage>() {

    override fun bindViewHolder(binding: ListExerciseImageItemBinding, item: ExerciseImage) {
        binding.apply {
            item.also {
                Glide.with(binding.root)
                    .load(it.photo.toUri().buildUpon().scheme("https").build())
                    .into(imageView)
            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ListExerciseImageItemBinding
        get() = { inflater, parent, _ ->
            ListExerciseImageItemBinding.inflate(inflater, parent, false)
        }
}