package com.example.minh.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.minh.domain.FireBaseState
import com.example.minh.domain.model.User
import com.example.minh.domain.usecase.stepcounter.GetStepCounterUseCase
import com.example.minh.domain.usecase.user.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


@HiltViewModel
class MainViewModel @Inject constructor(
    private val loginUserUseCase: LoginUserUseCase,
    private val registerUserUseCase: RegisterUserUseCase,
    private val registerBodyInformationUseCase: RegisterBodyInformationUseCase,
    private val getCurrentUserUseCase: GetCurrentUserUseCase,
    private val getStepCounterUseCase: GetStepCounterUseCase,
    private val logoutUseCase: LogoutUseCase
) : ViewModel() {

    private val _user = MutableStateFlow<FireBaseState<String>>(FireBaseState.Loading(""))
    val user: StateFlow<FireBaseState<String>> get() = _user

    private val _userInformation =
        MutableStateFlow<FireBaseState<String>>(FireBaseState.Loading(""))
    val userInformation: StateFlow<FireBaseState<String>> get() = _userInformation

    private val _userLogin =
        MutableStateFlow<FireBaseState<String>>(FireBaseState.Loading(""))
    val userLogin: StateFlow<FireBaseState<String>> get() = _userLogin

    private val _userData = MutableLiveData<User>()
    val userData: MutableLiveData<User> get() = _userData

    private val _stepTotal = MutableStateFlow(listOf(0f))
    val stepTotal: StateFlow<List<Float>> get() = _stepTotal

    fun register(userName: String, email: String, password: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _user.update {
                    registerUserUseCase(userName, email, password)
                }
            }
        }
    }

    fun login(email: String, password: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _userLogin.update {
                    loginUserUseCase(email, password)
                }
            }
        }
    }

    fun logout(){
        viewModelScope.launch {
            _userLogin.emit(FireBaseState.Fail("",""))
            logoutUseCase()
        }

    }

    fun registerBodyInformation(dateOfBirth: String, height: String, weight: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _userInformation.update {
                    registerBodyInformationUseCase(dateOfBirth, height, weight)
                }
            }
        }
    }

    fun getProfile() {
        CoroutineScope(Dispatchers.IO).launch {
            getCurrentUserUseCase().collect {
                _userData.postValue(it)
            }
        }
    }

    fun getStepValues() {
        CoroutineScope(Dispatchers.IO).launch {
            getStepCounterUseCase().collect {
                _stepTotal.value = it
            }
        }
    }
}