package com.example.minh.presentation.customview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.Paint.Align
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.example.minh.R
import com.example.minh.domain.model.Goal
import com.example.minh.domain.model.WorkoutLogging

@Suppress("SameParameterValue")
class CustomChartView(context: Context, attr: AttributeSet) : View(context, attr) {

    private var ratioColumn = listOf(
        WorkoutLogging.randomWorkoutLogging(),
        WorkoutLogging.randomWorkoutLogging(),
        WorkoutLogging.randomWorkoutLogging(),
        WorkoutLogging.randomWorkoutLogging(),
        WorkoutLogging.randomWorkoutLogging(),
        WorkoutLogging.randomWorkoutLogging(),
        WorkoutLogging.randomWorkoutLogging(),
    )

    private var spaceHeight = 0f
    private var widthRect = 0f
    private var spaceWidth = 0f

    @SuppressLint("DrawAllocation")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        spaceHeight = measuredHeight / 20f
        widthRect = 0.9f * measuredWidth / 13f
        spaceWidth = (0.9f * measuredWidth - widthRect * 7) / 6
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        setLines(canvas)        //set line
        setColumns(canvas)      //set columns
    }

    //custom draw text
    private fun customDrawText(
        canvas: Canvas?,
        text: String,
        posX: Float,
        posY: Float,
        textSizeCustom: Float,
        textColorCustom: Int,
        textStyleCustom: Paint.Style,
        textAlign: Align,
        textStrokeWidth: Float
    ) {
        val tPaint = TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
            color = textColorCustom
            textSize = textSizeCustom
            style = textStyleCustom
            setTextAlign(textAlign)
            strokeWidth = textStrokeWidth
        }
        canvas?.drawText(text, posX, posY, tPaint)
    }

    private fun setColumns(canvas: Canvas?) {
        val weekDays = listOf("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")

        for (i in 0..6) {
            val heightColumn =
                if ((ratioColumn[i].currentCalories / Goal.randomGoal().caloriesBurnPerDay!!).toFloat() >= 1f) {
                    1f
                } else {
                    ((ratioColumn[i].currentCalories / Goal.randomGoal().caloriesBurnPerDay!!).toFloat())
                }
            canvas?.drawRoundRect(
                (widthRect + spaceWidth) * i,
                heightColumn * measuredHeight ,
                (widthRect + spaceWidth) * i + widthRect,
                measuredHeight * 0.1f * 6 + spaceHeight * 5,
                10f,
                10f,
                Paint(Paint.ANTI_ALIAS_FLAG).apply {
                    style = Paint.Style.FILL_AND_STROKE
                    shader = LinearGradient(
                        (widthRect + spaceWidth) * i + widthRect / 2,
                        heightColumn * measuredHeight,
                        (widthRect + spaceWidth) * i + 3 * widthRect / 2,
                        measuredHeight * 0.1f * 6 + spaceHeight * 5,
                        ContextCompat.getColor(
                            context,
                            R.color.white
                        ),
                        ContextCompat.getColor(context, R.color.transparent),
                        Shader.TileMode.CLAMP
                    )
                })

            customDrawText(
                canvas,
                weekDays[i],
                (widthRect + spaceWidth) * i + widthRect / 2,
                0.95f * measuredHeight,
                20f,
                Color.WHITE,
                Paint.Style.FILL_AND_STROKE,
                Align.CENTER,
                2f
            )
        }
    }


    private fun setLines(canvas: Canvas?) {

        //percentage
        val percentage = listOf("100%", "80%", "60%", "40%", "20%", "0%")

        //LinePaint
        val linePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.FILL_AND_STROKE
            color = Color.WHITE
            strokeWidth = 2f
        }

        for (i in 1..6) {
            canvas?.drawLine(
                measuredWidth * 0f,
                measuredHeight * 0.1f * i + spaceHeight * (i - 1),
                0.9f * measuredWidth,
                measuredHeight * 0.1f * i + spaceHeight * (i - 1), linePaint
            )

            customDrawText(
                canvas,
                percentage[i - 1],
                0.95f * measuredWidth,
                measuredHeight * 0.1f * i + spaceHeight * (i - 1),
                20f,
                Color.WHITE,
                Paint.Style.FILL_AND_STROKE,
                Align.CENTER,
                2f
            )
        }
    }
}