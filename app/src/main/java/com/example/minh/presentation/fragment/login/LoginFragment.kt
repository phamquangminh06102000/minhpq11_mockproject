package com.example.minh.presentation.fragment.login

import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.minh.databinding.LoginScreenBinding
import com.example.minh.domain.FireBaseState
import com.example.minh.presentation.activity.MainActivity
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.MainViewModel
import com.example.minh.utils.TYPE_EMAIL
import com.example.minh.utils.TYPE_PASSWORD
import com.example.minh.utils.options
import com.example.minh.utils.safeNavigate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginFragment : BaseFragment<LoginScreenBinding>(LoginScreenBinding::inflate),
    View.OnClickListener {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModel() {
        lifecycleScope.launch {
            mainViewModel.userLogin.collectLatest {
                loginUser(it)
            }
        }
    }

    override fun bindView() {
        with(binding) {
            tvRegister.setOnClickListener(this@LoginFragment)
            btnLogin.setOnClickListener(this@LoginFragment)
        }
    }

    private fun loginUser(state: FireBaseState<String>) {
        when (state) {
            is FireBaseState.Success -> {
                (requireActivity() as MainActivity).binding.loadingPanel.visibility = View.GONE
                findNavController().safeNavigate(
                    LoginFragmentDirections.actionLoginFragmentToHomeFragment(),
                    options
                )
                Toast.makeText(requireContext(), "Login Success", Toast.LENGTH_SHORT).show()
            }

            is FireBaseState.Fail -> {
                (requireActivity() as MainActivity).binding.loadingPanel.visibility = View.GONE
                if (state.type == TYPE_EMAIL) {
                    binding.edtEmail.error = "Email can't be empty"
                }
                if (state.type == TYPE_PASSWORD) {
                    binding.edtPassword.error = "Password can't be empty"
                }
                Toast.makeText(requireContext(), state.msg.toString(), Toast.LENGTH_SHORT)
                    .show()
            }
            is FireBaseState.Loading -> {
            }
        }
    }

    override fun onClick(view: View?) {
        with(binding) {
            when (view) {
                tvRegister -> {
                    findNavController().safeNavigate(
                        LoginFragmentDirections.actionLoginFragmentToRegisterFragment(),
                        options
                    )
                }
                btnLogin -> {
                    (requireActivity() as MainActivity).binding.loadingPanel.visibility = View.VISIBLE
                    mainViewModel.login(edtEmail.text.toString(), edtPassword.text.toString())
                }
            }
        }
    }
}