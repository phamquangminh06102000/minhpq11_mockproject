package com.example.minh.presentation.customview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.LinearGradient
import android.graphics.Shader
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.example.minh.R


@SuppressLint("ViewConstructor", "AppCompatCustomView", "ResourceAsColor")
class CustomGradientTextView(context: Context, attrs: AttributeSet?) : AppCompatTextView(context, attrs) {

    private var startColor = R.color.startC
    private var endColor = R.color.endC

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.MyGradientTextView,
            0,
            0
        ).apply {
            try {
                startColor = getColor(R.styleable.MyGradientTextView_startColor, R.color.startC)
                endColor = getColor(R.styleable.MyGradientTextView_endColor, R.color.endC)
            } finally {
                recycle()
            }
        }
    }
    @SuppressLint("DrawAllocation")
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        if (changed) {
            paint.shader = LinearGradient(
                0f, 0f, width.toFloat(), height.toFloat(),
                ContextCompat.getColor(context,startColor),
                ContextCompat.getColor(context, endColor),
                Shader.TileMode.CLAMP
            )
        }
    }
}