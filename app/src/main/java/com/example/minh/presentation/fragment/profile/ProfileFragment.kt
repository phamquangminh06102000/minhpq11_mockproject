package com.example.minh.presentation.fragment.profile

import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.minh.data.local.sharepre.SharedPreferenceHelper
import com.example.minh.databinding.ProfileScreenBinding
import com.example.minh.domain.model.User
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.MainViewModel
import com.example.minh.utils.options
import com.example.minh.utils.safeNavigate
import dagger.hilt.android.AndroidEntryPoint
import java.util.Calendar

@AndroidEntryPoint
class ProfileFragment : BaseFragment<ProfileScreenBinding>(ProfileScreenBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        mainViewModel.userData.observe(viewLifecycleOwner) {
            updateProfile(it)
        }
    }

    override fun bindView() {
        super.bindView()
        with(binding) {
            btnLogOut.setOnClickListener {
                findNavController().safeNavigate(
                    ProfileFragmentDirections.actionProfileFragmentToSplashFragment(),
                    options
                )
                mainViewModel.logout()
            }
        }
    }

    private fun updateProfile(user: User?) {
        val currentAge =
            Calendar.getInstance().get(Calendar.YEAR) - user!!.dateOfBirth!!.substring(
                user.dateOfBirth!!.length - 4,
                user.dateOfBirth!!.length
            ).toInt()
        with(binding) {
            tvName.text = user.userName
            heightDetail.text = user.height.toString()
            weightDetail.text = user.weight.toString()
            ageDetail.text = currentAge.toString()
        }
    }
}