package com.example.minh.presentation.fragment.notification

import androidx.navigation.fragment.findNavController
import com.example.minh.databinding.NotificationScreenBinding
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.utils.options
import com.example.minh.utils.safeNavigate
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationFragment :
    BaseFragment<NotificationScreenBinding>(NotificationScreenBinding::inflate) {

    override fun bindView() {
        with(binding) {
            btnBack.setOnClickListener {
                findNavController().safeNavigate(
                    NotificationFragmentDirections.actionNotificationFragmentToHomeFragment(),
                    options
                )
            }
        }
    }
}