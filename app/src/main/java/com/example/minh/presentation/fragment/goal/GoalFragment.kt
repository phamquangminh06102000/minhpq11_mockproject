package com.example.minh.presentation.fragment.goal

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.minh.databinding.GoalScreenBinding
import com.example.minh.presentation.adapter.GoalAdapter
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.GoalViewModel
import com.example.minh.utils.options
import com.example.minh.utils.safeNavigate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class GoalFragment : BaseFragment<GoalScreenBinding>(GoalScreenBinding::inflate) {

    private val goalViewModel: GoalViewModel by activityViewModels()

    private lateinit var goalAdapter: GoalAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        goalViewModel.getGoalFromFirebase()

    }

    override fun bindView() {
        super.bindView()
        binding.btnAdd.setOnClickListener {
            findNavController().safeNavigate(
                GoalFragmentDirections.actionGoalFragmentToGoalAddFragment(),
                options
            )
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        goalAdapter = GoalAdapter()
        lifecycleScope.launch {
            goalViewModel.goalList.observe(viewLifecycleOwner) {
                goalAdapter.submitList(it)
                Log.d("Minh", "observeViewModel:  line = 40 $it")
                handleGoal()
            }
        }
    }

    private fun handleGoal() {
        with(binding) {
            recyclerViewGoal.apply {
                adapter = goalAdapter
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            }
            goalAdapter.setOnItemClickListener {
                val action = GoalFragmentDirections.actionGoalFragmentToGoalDetailFragment(it)
                findNavController().safeNavigate(action, options)
            }
        }
    }
}