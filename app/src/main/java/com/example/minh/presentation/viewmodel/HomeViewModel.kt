package com.example.minh.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.minh.domain.model.Exercise
import com.example.minh.domain.model.ExerciseImage
import com.example.minh.domain.usecase.exercise.GetExercisesImageUseCase
import com.example.minh.domain.usecase.exercise.GetExercisesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getExercisesUseCase: GetExercisesUseCase,
    private val getExercisesImageUseCase: GetExercisesImageUseCase
) : ViewModel() {
    private val _exerciseData = MutableLiveData<List<Exercise>>()
    val exerciseData: LiveData<List<Exercise>> get() = _exerciseData

    private val _exerciseImage = MutableLiveData<List<ExerciseImage>>()
    val exerciseImage: LiveData<List<ExerciseImage>> get() = _exerciseImage

    fun getExercises() {
        CoroutineScope(Dispatchers.IO).launch {
            getExercisesUseCase().collect {
                _exerciseData.postValue(it)
            }
        }
    }

    fun getExerciseImage() {
        CoroutineScope(Dispatchers.IO).launch {
            getExercisesImageUseCase().collect {
                _exerciseImage.postValue(it)
            }
        }
    }
}