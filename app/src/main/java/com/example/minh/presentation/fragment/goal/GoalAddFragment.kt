package com.example.minh.presentation.fragment.goal

import android.app.DatePickerDialog
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.minh.databinding.AddGoalScreenBinding
import com.example.minh.domain.model.Goal
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.GoalViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class GoalAddFragment : BaseFragment<AddGoalScreenBinding>(AddGoalScreenBinding::inflate) {

    private val goalViewModel: GoalViewModel by activityViewModels()

    override fun observeViewModel() {
        super.observeViewModel()
    }


    override fun bindView() {
        super.bindView()
        with(binding) {
            btnSave.setOnClickListener {
                goalViewModel.addGoalToFirebase(
                    Goal(
                        goalName = binding.goalName.text.toString(),
                        goalDes = binding.goalDes.text.toString(),
                        target = binding.target.text.toString(),
                        duration = binding.duration.text.toString(),
                        stepsPerDay = binding.targetStep.text.toString().toInt(),
                        targetWeight = binding.targetWeight.text.toString().toDouble(),
                        targetDate = binding.targetDate.text.toString(),
                        caloriesBurnPerDay = binding.targetCalories.text.toString().toDouble(),
                    )
                )
                findNavController().popBackStack()
            }
            targetDate.setOnClickListener {
                DatePickerDialog(requireActivity()).run {
                    setOnDateSetListener { _, year, month, dayOfMonth ->
                        val dateOfBirth = "$dayOfMonth/${month + 1}/$year"
                        targetDate.setText(dateOfBirth)
                    }
                    show()
                }
            }
        }
    }
}