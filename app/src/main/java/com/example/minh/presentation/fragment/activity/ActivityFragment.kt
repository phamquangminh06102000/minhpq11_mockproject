package com.example.minh.presentation.fragment.activity

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.example.minh.databinding.ActivityScreenBinding
import com.example.minh.domain.model.Goal
import com.example.minh.domain.model.User
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.GoalViewModel
import com.example.minh.presentation.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ActivityFragment : BaseFragment<ActivityScreenBinding>(ActivityScreenBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()

    private val goalViewModel: GoalViewModel by activityViewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            mainViewModel.stepTotal.collectLatest {
                updateStepCounter(it)
            }

        }
    }

    override fun bindView() {
        super.bindView()
        mainViewModel.getProfile()
    }

    private fun updateStepCounter(step: List<Float>) {
        with(binding) {
            circularProgressBarStep.apply {
                setProgressWithAnimation(step[0])
            }
            tvCounter.text = step[0].toInt().toString()
            tvCalories.text = (step[0] * 0.04).toInt().toString()
        }
    }
}