package com.example.minh.presentation.fragment.register

import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.minh.databinding.RegisterScreenBinding
import com.example.minh.domain.FireBaseState
import com.example.minh.domain.model.User
import com.example.minh.presentation.activity.MainActivity
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.MainViewModel
import com.example.minh.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
@Suppress("UNUSED_EXPRESSION")
class RegisterFragment : BaseFragment<RegisterScreenBinding>(RegisterScreenBinding::inflate),
    View.OnClickListener {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModel() {
        lifecycleScope.launch {
            mainViewModel.user.collectLatest {
                registerUser(it)
            }
        }
    }

    override fun bindView() {
        with(binding) {
            btnRegister.setOnClickListener(this@RegisterFragment)
            tvLogin.setOnClickListener(this@RegisterFragment)
        }
    }

    private fun registerUser(state: FireBaseState<String>) {
        when (state) {
            is FireBaseState.Success -> {
                (requireActivity() as MainActivity).binding.loadingPanel.visibility = View.GONE
                Toast.makeText(requireContext(), state.msg.toString(), Toast.LENGTH_SHORT).show()
                findNavController().safeNavigate(
                    RegisterFragmentDirections.actionRegisterFragmentToBodyFragment(),
                    options
                )
            }

            is FireBaseState.Fail -> {
                (requireActivity() as MainActivity).binding.loadingPanel.visibility = View.GONE
                if (state.type == TYPE_USER_NAME) {
                    binding.edtUserName.error = "Name can't be empty or Name is invalid"
                }
                if (state.type == TYPE_EMAIL) {
                    binding.edtEmail.error = "Email can't be empty or Email is invalid"
                }
                if (state.type == TYPE_PASSWORD) {
                    binding.edtPassword.error = "Password can't be empty or Password is invalid"
                }
                Toast.makeText(requireContext(), state.msg.toString(), Toast.LENGTH_SHORT).show()
            }
            is FireBaseState.Loading -> {

            }
        }
    }

    override fun onClick(view: View?) {
        with(binding) {
            val userName = edtUserName.text.toString()
            val email = edtEmail.text.toString()
            val password = edtPassword.text.toString()
            val user = User(userName, email, password)
            when (view) {
                btnRegister -> {
                    (requireActivity() as MainActivity).binding.loadingPanel.visibility = View.VISIBLE
                    mainViewModel.register(userName, email, password)}
                tvLogin -> findNavController().safeNavigate(
                    RegisterFragmentDirections.actionRegisterFragmentToLoginFragment(),
                    options
                )
            }
        }
    }
}