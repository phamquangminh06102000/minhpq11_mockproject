package com.example.minh.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.minh.databinding.ListGoalItemBinding
import com.example.minh.domain.model.Goal
import com.example.minh.presentation.base.BaseAdapter

class GoalAdapter : BaseAdapter<ListGoalItemBinding, Goal>() {

    @SuppressLint("SetTextI18n")
    override fun bindViewHolder(binding: ListGoalItemBinding, item: Goal) {
        binding.apply {
            item.also {
                tvNameGoal.text = "Name: ${it.goalName}"
                tvDesGoal.text = "Description:${it.goalDes}"
                tvTarget.text = "Target: ${it.target}"
                tvDuration.text = "Duration: ${it.duration}"
            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ListGoalItemBinding
        get() = { inflater, parent, _ ->
            ListGoalItemBinding.inflate(inflater, parent, false)
        }
}