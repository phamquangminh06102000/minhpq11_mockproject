package com.example.minh.presentation.fragment.register

import android.app.DatePickerDialog
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.minh.databinding.BodyScreenBinding
import com.example.minh.domain.FireBaseState
import com.example.minh.domain.model.User
import com.example.minh.presentation.activity.MainActivity
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.MainViewModel
import com.example.minh.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
@Suppress("UNUSED_EXPRESSION")
class BodyFragment : BaseFragment<BodyScreenBinding>(BodyScreenBinding::inflate),
    View.OnClickListener {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            mainViewModel.userInformation.collectLatest {
                registerBodyUser(it)
            }
        }
    }

    private fun registerBodyUser(state: FireBaseState<String>) {
        when (state) {
            is FireBaseState.Success -> {
                (requireActivity() as MainActivity).binding.loadingPanel.visibility = View.GONE
                findNavController().safeNavigate(
                    BodyFragmentDirections.actionBodyFragmentToLoginFragment(),
                    options
                )
            }
            is FireBaseState.Fail -> {
                (requireActivity() as MainActivity).binding.loadingPanel.visibility = View.GONE
                if (state.type == TYPE_DATEOFBIRTH){
                    binding.edtBOD.error = "Birthday is\'n empty"
                }
                if (state.type == TYPE_HEIGHT){
                    binding.edtHeight.error = "Height is\'n empty"
                }
                if (state.type == TYPE_WEIGHT){
                    binding.edtWeight.error = "Weight is\'n empty"
                }
                Toast.makeText(requireContext(), state.msg.toString(), Toast.LENGTH_SHORT).show()
            }
            is FireBaseState.Loading -> {
            }
        }
    }

    override fun onClick(view: View?) {
        with(binding) {
            val dob = edtBOD.text.toString()
            val height = edtHeight.text.toString()
            val weight = edtWeight.text.toString()
            val user = User(dob, height, weight)
            when (view) {
                btnNext -> {
                    (requireActivity() as MainActivity).binding.loadingPanel.visibility = View.VISIBLE
                    mainViewModel.registerBodyInformation(dob, height, weight)
                }
                edtBOD -> {
                    DatePickerDialog(requireActivity()).run {
                        setOnDateSetListener { _, year, month, dayOfMonth ->
                            val dateOfBirth = "$dayOfMonth/${month + 1}/$year"
                            edtBOD.setText(dateOfBirth)
                        }
                        show()
                    }
                }
            }
        }
    }

    override fun bindView() {
        with(binding) {
            btnNext.setOnClickListener(this@BodyFragment)
            edtBOD.setOnClickListener(this@BodyFragment)
        }
    }
}