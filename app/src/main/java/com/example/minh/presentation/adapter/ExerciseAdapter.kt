package com.example.minh.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.example.minh.databinding.ListExcerciseItemBinding
import com.example.minh.domain.model.Exercise
import com.example.minh.presentation.base.BaseAdapter

class ExerciseAdapter : BaseAdapter<ListExcerciseItemBinding, Exercise>() {

    override fun bindViewHolder(binding: ListExcerciseItemBinding, item: Exercise) {
        binding.apply {
            item.also {
                tvNameExercise.text = it.name
                tvDesExercise.text = it.type
                Glide.with(binding.root)
                    .load(it.photo.toUri().buildUpon().scheme("https").build())
                    .into(imageExercise)
            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ListExcerciseItemBinding
        get() = { inflater, parent, _ ->
            ListExcerciseItemBinding.inflate(inflater, parent, false)
        }
}