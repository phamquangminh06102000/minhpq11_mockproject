package com.example.minh.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.minh.domain.model.Goal
import com.example.minh.domain.usecase.goal.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GoalViewModel @Inject constructor(
    private val saveToDatabaseUseCase: SaveToDatabaseUseCase,
    private val addGoalUseCase: AddGoalUseCase,
    private val updateGoalUseCase: UpdateGoalUseCase,
    private val deleteGoalUseCase: DeleteGoalUseCase,
    private val getGoalUseCase: GetGoalUseCase
) : ViewModel() {

    private val _goalList = MutableLiveData<List<Goal>>()
    val goalList: LiveData<List<Goal>> get() = _goalList

    fun getGoalFromFirebase() {
        viewModelScope.launch(Dispatchers.IO) {
            val job1 = async {
                getGoalUseCase.fromFirebase().collect {
                    saveToDatabaseUseCase(it)
                    Log.d("Minh", "getGoalFromFirebase:  line = 31 $it")
                }
            }

            val job2 = async {
                getGoalUseCase.fromLocal().collect {
                    Log.d("Minh", "getGoalFromFirebase:  line = 32 $it")
                    _goalList.postValue(it)
                }
            }

            job1.await()
            job2.await()


        }
    }

    fun getGoalFromLocal() {
        viewModelScope.launch(Dispatchers.IO) {
            getGoalUseCase.fromLocal().collect {
                _goalList.value = it
            }
        }
    }

    fun addGoalToFirebase(goal: Goal) {
        viewModelScope.launch(Dispatchers.IO) {
            addGoalUseCase.fromFirebase(goal)
        }
    }

    fun updateGoalToFirebase(goal: Goal) {
        viewModelScope.launch(Dispatchers.IO) {
            updateGoalUseCase.fromFirebase(goal)
        }
    }

    fun deleteGoalFromFirebase(goal: Goal) {
        viewModelScope.launch(Dispatchers.IO) {
            deleteGoalUseCase.fromFirebase(goal)
        }
    }

    fun addGoalToLocal(goal: Goal) {
        viewModelScope.launch(Dispatchers.IO) {
            addGoalUseCase.fromLocal(goal)
        }
    }

    fun deleteGoalFromLocal(goal: Goal) {
        viewModelScope.launch(Dispatchers.IO) {
            deleteGoalUseCase.fromLocal(goal)
        }
    }

    fun updateGoalToLocal(goal: Goal) {
        viewModelScope.launch(Dispatchers.IO) {
            updateGoalUseCase.fromLocal(goal)
        }
    }
}
