package com.example.minh.presentation

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.minh.domain.usecase.user.UpdateDailyWorkoutLoggingUseCase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@AndroidEntryPoint
class WorkoutLoggingReceiver : BroadcastReceiver() {

    @Inject
    lateinit var updateDailyWorkoutLoggingUseCase: UpdateDailyWorkoutLoggingUseCase

    override fun onReceive(context: Context, intent: Intent?) {
        CoroutineScope(Dispatchers.IO).launch {
            updateDailyWorkoutLoggingUseCase()
        }
    }
}
