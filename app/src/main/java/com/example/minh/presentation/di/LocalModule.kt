package com.example.minh.presentation.di

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.example.minh.data.local.goal.GoalDao
import com.example.minh.data.local.goal.GoalDatabase
import com.example.minh.utils.MY_SHARED_PREFERENCE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LocalModule {

    @Provides
    @Singleton
    fun provideGoalDao(goalDatabase: GoalDatabase): GoalDao {
        return goalDatabase.getGoalDao()
    }

    @Provides
    @Singleton
    fun provideUserDatabase(@ApplicationContext applicationContext: Context): GoalDatabase {
        return Room.databaseBuilder(
            applicationContext,
            GoalDatabase::class.java,
            "user"
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideSharedPreference(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(MY_SHARED_PREFERENCE, Context.MODE_PRIVATE)
    }

}