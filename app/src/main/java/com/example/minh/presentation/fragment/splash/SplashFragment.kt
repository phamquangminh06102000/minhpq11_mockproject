package com.example.minh.presentation.fragment.splash

import android.util.Log
import android.view.animation.AnimationUtils
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.minh.R
import com.example.minh.data.local.sharepre.SharedPreferenceHelper
import com.example.minh.databinding.SplashScreenBinding
import com.example.minh.domain.FireBaseState
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.MainViewModel
import com.example.minh.utils.options
import com.example.minh.utils.safeNavigate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment : BaseFragment<SplashScreenBinding>(SplashScreenBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()

    @Inject
    lateinit var sharedPreferenceHelper: SharedPreferenceHelper

    override fun bindView() {
        super.bindView()
        setAnimation()
        checkLogin()
    }

    private fun checkLogin() {
        lifecycleScope.launch {
            delay(2000L)
            if (sharedPreferenceHelper.getIsOpenFirstTime()) {
//                Handler(Looper.getMainLooper()).postDelayed({
//                    findNavController().navigate(
//                        R.id.action_splashFragment_to_welcomeFragment,
//                        null,
//                        options
//                    )
//                }, 3500)
                findNavController().safeNavigate(
                    SplashFragmentDirections.actionSplashFragmentToWelcomeFragment(),
                    options
                )
            }
            if (!sharedPreferenceHelper.getIsLoggedIn() && !sharedPreferenceHelper.getIsRemember()) {
                findNavController().safeNavigate(
                    SplashFragmentDirections.actionSplashFragmentToLoginFragment(),
                    options
                )
                Log.d("Minh", "checkLogin:  line = 56")
                mainViewModel.logout()
            } else {
                mainViewModel.login(
                    sharedPreferenceHelper.getEmail()!!,
                    sharedPreferenceHelper.getPassword()!!
                )
            }
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            mainViewModel.userLogin.collectLatest {
                handleUserLogin(it)
            }
        }
    }

    private fun handleUserLogin(state: FireBaseState<String>) {
        when (state) {
            is FireBaseState.Success -> {
                if (sharedPreferenceHelper.getIsLoggedIn()) {
                    mainViewModel.getProfile()
                    findNavController().safeNavigate(
                        SplashFragmentDirections.actionSplashFragmentToHomeFragment(), options
                    )
                }
            }
            is FireBaseState.Fail -> {
                sharedPreferenceHelper.setIsLoggedIn(false)
            }
            else -> {}
        }
    }

    private fun setAnimation() {
        with(binding) {
            imageLogo.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.splash_scree_aim
                )
            )

            imageText.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide_in_left
                )
            )

            textTitle.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide_in_left
                )
            )
        }
    }
}


