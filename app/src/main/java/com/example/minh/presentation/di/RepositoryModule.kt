package com.example.minh.presentation.di

import com.example.minh.data.local.goal.GoalDao
import com.example.minh.data.network.api.exerciseapi.ExerciseMockApiDataSource
import com.example.minh.data.network.api.exerciseimageapi.ExerciseImageMockApiDataSource
import com.example.minh.data.network.firebase.goal.GoalDataSource
import com.example.minh.data.network.firebase.user.UserDataSource
import com.example.minh.data.repository.*
import com.example.minh.domain.repository.ExerciseImageRepositpry
import com.example.minh.domain.repository.ExerciseRepository
import com.example.minh.domain.repository.GoalRepository
import com.example.minh.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class GoalRepositoryFirebase

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class GoalRepositoryLocal


    @Provides
    @Singleton
    fun provideUserRepository(userDataSource: UserDataSource): UserRepository {
        return UserRepositoryImpl(userDataSource)
    }

    @Provides
    @Singleton
    fun provideExerciseRepository(exerciseMockApiDataSource: ExerciseMockApiDataSource): ExerciseRepository {
        return ExerciseRepositoryImpl(exerciseMockApiDataSource)
    }

    @Provides
    @GoalRepositoryFirebase
    fun provideGoalRepositoryFirebase(goalDataSource: GoalDataSource): GoalRepository {
        return GoalRepositoryFirebaseImpl(goalDataSource)
    }

    @Provides
    @GoalRepositoryLocal
    fun provideGoalRepositoryLocal(goalDao: GoalDao): GoalRepository {
        return GoalRepositoryLocalImpl(goalDao)
    }

    @Provides
    @Singleton
    fun provideExerciseImageRepository(exerciseImageMockApiDataSource: ExerciseImageMockApiDataSource): ExerciseImageRepositpry {
        return ExerciseImageRepositoryImpl(exerciseImageMockApiDataSource)
    }
}