package com.example.minh.presentation.fragment.stepcounter

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.example.minh.databinding.StepcounterScreenBinding
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class StepCounterFragment :
    BaseFragment<StepcounterScreenBinding>(StepcounterScreenBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            mainViewModel.stepTotal.collectLatest {
                updateStepTotal(it)
            }
        }
    }

    private fun updateStepTotal(step: List<Float>) {
        with(binding) {
            circularProgressBar.apply {
                setProgressWithAnimation(step[0])
            }
            tvCounter.text = step[0].toInt().toString()
        }
    }

    override fun bindView() {
        super.bindView()
        mainViewModel.getStepValues()
    }
}