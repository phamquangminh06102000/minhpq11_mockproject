package com.example.minh.presentation.customview

import android.content.Context
import android.graphics.*
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.example.minh.R

class CustomRectangle(context: Context, attrs: AttributeSet) : View(context, attrs) {

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        drawColumn(canvas)
    }

//    private fun drawRectangleBorder(canvas: Canvas?) {
//
//        val rect = RectF(0f, 0f, measuredWidth.toFloat(), measuredHeight.toFloat())
//
//        val borderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
//            color = Color.BLACK
//            style = Paint.Style.STROKE
//            strokeWidth = 5f
//        }
//
//        val insidePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
//            style = Paint.Style.FILL_AND_STROKE
//            shader = LinearGradient(
//                0f, 0f, measuredWidth.toFloat(), measuredHeight.toFloat(),
//                ContextCompat.getColor(context, R.color.white_pink),
//                ContextCompat.getColor(context, R.color.endC),
//                Shader.TileMode.CLAMP
//            )
//        }
//
//        canvas?.drawRoundRect(rect, 20f, 20f, borderPaint)
//        canvas?.drawRoundRect(rect, 20f,20f, insidePaint)
//    }

    private fun drawColumn(canvas: Canvas?) {
        val rect = RectF(
            0.05f * measuredWidth,
            0.1f * measuredHeight,
            0.15f * measuredWidth,
            0.9f * measuredHeight
        )

        val rectSleep = RectF(
            0.2f * measuredWidth,
            0.1f * measuredHeight,
            0.45f * measuredWidth,
            0.9f * measuredHeight
        )

        val rectStep = RectF(
            0.55f * measuredWidth,
            0.1f * measuredHeight,
            0.95f * measuredWidth,
            0.45f * measuredHeight
        )

        val rectCalories = RectF(
            0.55f * measuredWidth,
            0.55f * measuredHeight,
            0.95f * measuredWidth,
            0.9f * measuredHeight
        )




        val columnPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            shader = LinearGradient(
                0f, 0f, measuredWidth.toFloat(), measuredHeight.toFloat(),
                ContextCompat.getColor(context, R.color.white_pink),
                ContextCompat.getColor(context, R.color.endC),
                Shader.TileMode.CLAMP
            )
            style = Paint.Style.STROKE
            strokeWidth = 20f
        }

        canvas?.drawRoundRect(rect, 35f, 35f, columnPaint)
        canvas?.drawRoundRect(rectSleep, 25f, 25f, columnPaint)
        canvas?.drawOval(rectCalories, columnPaint)
        canvas?.drawOval(rectStep, columnPaint)
    }
}