package com.example.minh.presentation.fragment.home

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.minh.R
import com.example.minh.databinding.HomeScreenBinding
import com.example.minh.domain.model.User
import com.example.minh.presentation.activity.MainActivity
import com.example.minh.presentation.adapter.ExerciseAdapter
import com.example.minh.presentation.adapter.ExerciseImageAdapter
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.HomeViewModel
import com.example.minh.presentation.viewmodel.MainViewModel
import com.example.minh.utils.options
import com.example.minh.utils.safeNavigate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeScreenBinding>(HomeScreenBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()
    private val honeViewModel: HomeViewModel by viewModels()

    private lateinit var exerciseAdapter: ExerciseAdapter

    private lateinit var exerciseImageAdapter: ExerciseImageAdapter

    private var backPressedTime: Long = 0
    lateinit var backToast: Toast

    override fun observeViewModel() {
        lifecycleScope.launch {
            mainViewModel.userData.observe(viewLifecycleOwner) {
                handleLoginResult(it)
            }
            exerciseAdapter = ExerciseAdapter()
            honeViewModel.exerciseData.observe(viewLifecycleOwner) {
                Log.d("Minh", "observeViewModel:  line = 38")
                if (it.isEmpty()) {
                    (requireActivity() as MainActivity).binding.loadingPanel.visibility =
                        View.VISIBLE
                    Log.d("Minh", "observeViewModel:  line = 40")
                } else {
                    exerciseAdapter.submitList(it)
                    handleExerciseData()
                }
            }
            exerciseImageAdapter = ExerciseImageAdapter()
            honeViewModel.exerciseImage.observe(viewLifecycleOwner) {
                Log.d("Minh", "observeViewModel:  line = 54 $it")
                if (it.isEmpty()) {
                    (requireActivity() as MainActivity).binding.loadingPanel.visibility =
                        View.VISIBLE
                    Log.d("Minh", "observeViewModel:  line = 40")
                } else {
                    exerciseImageAdapter.submitList(it)
                    handleExerciseImage()
                }
            }
        }
    }

    private fun handleExerciseImage() {
        with(binding) {
            recyclerViewExerciseImage.apply {
                adapter = exerciseImageAdapter
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            }
        }
    }

    private fun handleExerciseData() {
        with(binding) {
            recyclerViewExercise.apply {
                adapter = exerciseAdapter
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                exerciseAdapter.setOnItemClickListener {
                    val action =
                        HomeFragmentDirections.actionHomeFragmentToDetailExerciseFragment(it)
                    findNavController().safeNavigate(action, options)
                }
            }
        }
    }


    private fun handleLoginResult(user: User) {
        with(binding) {
            tvUserName.text = user.userName
        }
    }

    override fun bindView() {
        (requireActivity() as MainActivity).binding.bottomNavigation.isVisible = true
        pressAgainToExit()
        with(binding) {
            btnNotify.setOnClickListener {
                findNavController().navigate(
                    R.id.action_homeFragment_to_notificationFragment,
                    null,
                    options
                )
            }
        }
        mainViewModel.getProfile()
        honeViewModel.getExercises()
        honeViewModel.getExerciseImage()
    }

    private fun pressAgainToExit() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    backToast = Toast.makeText(
                        requireContext(),
                        "Press back again to leave the app.",
                        Toast.LENGTH_LONG
                    )
                    if (backPressedTime + 2000 > System.currentTimeMillis()) {
                        backToast.cancel()
                        requireActivity().finish()
                        return
                    } else {
                        backToast.show()
                    }
                    backPressedTime = System.currentTimeMillis()
                }
            })
    }

}

