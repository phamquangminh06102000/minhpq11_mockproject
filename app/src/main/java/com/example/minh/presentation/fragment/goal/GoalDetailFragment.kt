package com.example.minh.presentation.fragment.goal

import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.minh.databinding.DetailGoalScreenBinding
import com.example.minh.domain.model.Goal
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.presentation.viewmodel.GoalViewModel
import com.example.minh.utils.options
import com.example.minh.utils.safeNavigate
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class GoalDetailFragment : BaseFragment<DetailGoalScreenBinding>(DetailGoalScreenBinding::inflate) {

    private val goalViewModel: GoalViewModel by activityViewModels()

    private val args by navArgs<GoalDetailFragmentArgs>()

    override fun bindView() {
        super.bindView()
        with(binding) {
            tvNameGoal.setText(args.goalDetail.goalName)
            tvDesGoal.setText(args.goalDetail.goalDes)
            tvTarget.setText(args.goalDetail.target)
            tvDuration.setText(args.goalDetail.duration)
            tvStep.setText(args.goalDetail.stepsPerDay.toString())
            tvTargetWeight.setText(args.goalDetail.targetWeight.toString())
            tvCalories.setText(args.goalDetail.caloriesBurnPerDay.toString())
            tvTargetDate.setText(args.goalDetail.stepsPerDay.toString())
            btnSave.setOnClickListener {
                goalViewModel.updateGoalToFirebase(
                    Goal(
                        goalId = args.goalDetail.goalId,
                        goalName = tvNameGoal.text.toString(),
                        goalDes = tvDesGoal.text.toString(),
                        target = tvTarget.text.toString(),
                        duration = tvDuration.text.toString(),
                        stepsPerDay = tvStep.text.toString().toInt(),
                        targetWeight = tvTargetWeight.text.toString().toDouble(),
                        targetDate = tvTargetDate.text.toString(),
                        caloriesBurnPerDay = tvCalories.text.toString().toDouble(),
                    )
                )
                goalViewModel.updateGoalToLocal(
                    Goal(
                        goalId = args.goalDetail.goalId,
                        goalName = tvNameGoal.text.toString(),
                        goalDes = tvDesGoal.text.toString(),
                        target = tvTarget.text.toString(),
                        duration = tvDuration.text.toString(),
                        stepsPerDay = tvStep.text.toString().toInt(),
                        targetWeight = tvTargetWeight.text.toString().toDouble(),
                        targetDate = tvTargetDate.text.toString(),
                        caloriesBurnPerDay = tvCalories.text.toString().toDouble(),
                    )
                )
                findNavController().safeNavigate(
                    GoalDetailFragmentDirections.actionGoalDetailFragmentToGoalFragment(),
                    options
                )
            }
            btnDelete.setOnClickListener {
                goalViewModel.deleteGoalFromFirebase(
                    Goal(
                        goalId = args.goalDetail.goalId,
                        goalName = tvNameGoal.text.toString(),
                        goalDes = tvDesGoal.text.toString(),
                        target = tvTarget.text.toString(),
                        duration = tvDuration.text.toString(),
                        stepsPerDay = tvStep.text.toString().toInt(),
                        targetWeight = tvTargetWeight.text.toString().toDouble(),
                        targetDate = tvTargetDate.text.toString(),
                        caloriesBurnPerDay = tvCalories.text.toString().toDouble(),
                    )
                )
                goalViewModel.deleteGoalFromLocal(
                    Goal(
                        goalId = args.goalDetail.goalId,
                        goalName = tvNameGoal.text.toString(),
                        goalDes = tvDesGoal.text.toString(),
                        target = tvTarget.text.toString(),
                        duration = tvDuration.text.toString(),
                        stepsPerDay = tvStep.text.toString().toInt(),
                        targetWeight = tvTargetWeight.text.toString().toDouble(),
                        targetDate = tvTargetDate.text.toString(),
                        caloriesBurnPerDay = tvCalories.text.toString().toDouble(),
                    )
                )
                findNavController().safeNavigate(
                    GoalDetailFragmentDirections.actionGoalDetailFragmentToGoalFragment(),
                    options
                )
            }
        }
    }
}