package com.example.minh.presentation.di

import com.example.minh.domain.repository.GoalRepository
import com.example.minh.domain.usecase.goal.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {

    @Provides
    @Singleton
    fun provideSaveToDatabaseUseCase(
        @RepositoryModule.GoalRepositoryLocal goalRepositoryLocal: GoalRepository
    ): SaveToDatabaseUseCase {
        return SaveToDatabaseUseCase(goalRepositoryLocal)
    }


    @Provides
    @Singleton
    fun provideAddGoalUseCase(
        @RepositoryModule.GoalRepositoryLocal goalRepositoryLocal: GoalRepository,
        @RepositoryModule.GoalRepositoryFirebase goalRepositoryFirebase: GoalRepository
    ): AddGoalUseCase {
        return AddGoalUseCase(goalRepositoryLocal, goalRepositoryFirebase)
    }

    @Provides
    @Singleton
    fun provideUpdateGoalUseCase(
        @RepositoryModule.GoalRepositoryLocal goalRepositoryLocal: GoalRepository,
        @RepositoryModule.GoalRepositoryFirebase goalRepositoryFirebase: GoalRepository
    ): UpdateGoalUseCase {
        return UpdateGoalUseCase(goalRepositoryLocal, goalRepositoryFirebase)
    }

    @Provides
    @Singleton
    fun provideDeleteGoalUseCase(
        @RepositoryModule.GoalRepositoryLocal goalRepositoryLocal: GoalRepository,
        @RepositoryModule.GoalRepositoryFirebase goalRepositoryFirebase: GoalRepository
    ): DeleteGoalUseCase {
        return DeleteGoalUseCase(goalRepositoryLocal, goalRepositoryFirebase)
    }

    @Provides
    @Singleton
    fun provideGetGoalUseCase(
        @RepositoryModule.GoalRepositoryLocal goalRepositoryLocal: GoalRepository,
        @RepositoryModule.GoalRepositoryFirebase goalRepositoryFirebase: GoalRepository
    ): GetGoalUseCase {
        return GetGoalUseCase(goalRepositoryLocal, goalRepositoryFirebase)
    }
}