package com.example.minh.presentation.customview

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.text.TextPaint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.minh.R
import com.example.minh.utils.TAG

@SuppressLint("CustomSplashScreen")
class CustomSplashScreen(context: Context, attrs: AttributeSet) : View(context, attrs) {

    private var progressValue: ValueAnimator = ValueAnimator.ofInt(0, 100)
    private var progressLoad = 0

    private var progressBar: ValueAnimator = ValueAnimator.ofFloat(1f, 9f)
    private var progressLoadBar = 0f

    private var progressLoadIcon: ValueAnimator = ValueAnimator.ofFloat(1f,7f)
    private var progressLoadBarIcon = 0f

    @Volatile
    private var colorAnimator: ValueAnimator

    private var originColor = Color.CYAN
    private var afterColor = Color.rgb(255, 182, 193)

    private var animatorPaint = 0

    init {
        progressValue.apply {
            duration = 2000
            addUpdateListener {
                progressLoad = it.animatedValue as Int
                invalidate()
                requestLayout()
            }
            start()
        }

        progressBar.apply {
            duration = 2000
            addUpdateListener {
                progressLoadBar = it.animatedValue as Float
                invalidate()
                requestLayout()
            }
            start()
        }

        progressLoadIcon.apply {
            duration = 2000
            addUpdateListener {
                progressLoadBarIcon = it.animatedValue as Float
                invalidate()
                requestLayout()
            }
            start()
        }

        colorAnimator = ValueAnimator.ofObject(ArgbEvaluator(), originColor, afterColor)
        colorAnimator.duration = 2000
        colorAnimator.addUpdateListener {
            animatorPaint = it.animatedValue as Int
            invalidate()
            requestLayout()
        }
        colorAnimator.start()
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        Log.d(TAG, "onMeasure: $measuredHeight, $measuredWidth")
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        setDynLogo(canvas, R.drawable.exercise)
        setLoading(canvas)
    }

    private fun setDynLogo(canvas: Canvas?, resID: Int) {
        val iconDynLogo =
            ResourcesCompat.getDrawable(resources, resID, null)
                ?.toBitmap((0.2f * measuredWidth).toInt(), (0.1f * measuredHeight).toInt(), null)
        if (iconDynLogo != null) {
            canvas?.drawBitmap(iconDynLogo, 0.1f * measuredWidth * progressLoadBarIcon, 0.7f * measuredHeight, null)
        }
    }

    private fun setLoading(canvas: Canvas?) {
        val paintLoading = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.FILL_AND_STROKE
            color = animatorPaint
        }

        val paintBackLoading = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.FILL_AND_STROKE
            color = Color.LTGRAY
        }

        val rectLoading = RectF(
            0.1f * measuredWidth,
            0.8f * measuredHeight,
            0.1f * measuredWidth * progressLoadBar,
            0.82f * measuredHeight
        )

        val rectBackLoading = RectF(
            0.1f * measuredWidth,
            0.8f * measuredHeight,
            0.9f * measuredWidth,
            0.82f * measuredHeight
        )
        canvas?.drawRoundRect(rectBackLoading, 20f, 20f, paintBackLoading)
        canvas?.drawRoundRect(rectLoading, 20f, 20f, paintLoading)
        canvas?.drawText(
            "${progressLoad}%",
            0.5f * measuredWidth,
            0.82f * measuredHeight,
            TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
                style = Paint.Style.FILL_AND_STROKE
                color = Color.BLACK
                textSize = 50f
                textAlign = Paint.Align.CENTER
            })
    }
}