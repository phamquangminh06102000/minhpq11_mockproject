package com.example.minh.presentation.di

import android.content.Context
import com.example.minh.data.local.stepcounter.MeasurableSensor
import com.example.minh.data.local.stepcounter.StepSensor
import com.example.minh.data.repository.SensorRepositoryImpl
import com.example.minh.domain.repository.SensorRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object SensorModule {

    @Provides
    @Singleton
    fun provideStepSensor(@ApplicationContext context: Context): MeasurableSensor {
        return StepSensor(context)
    }

    @Provides
    @Singleton
    fun provideSensorDataSource(measurableSensor: MeasurableSensor): SensorRepository {
        return SensorRepositoryImpl(measurableSensor)
    }
}