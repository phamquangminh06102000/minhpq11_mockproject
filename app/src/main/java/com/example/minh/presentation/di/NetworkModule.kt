package com.example.minh.presentation.di

import com.example.minh.data.network.firebase.goal.GoalDataSource
import com.example.minh.data.network.firebase.goal.GoalDataSourceImpl
import com.example.minh.data.network.firebase.user.UserDataSource
import com.example.minh.data.network.firebase.user.UserDataSourceImpl
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth {
        return Firebase.auth
    }

    @Provides
    @Singleton
    fun provideUserDataSource(
        firebaseAuth: FirebaseAuth,
        databaseReference: DatabaseReference
    ): UserDataSource {
        return UserDataSourceImpl(firebaseAuth, databaseReference)
    }

    @Provides
    @Singleton
    fun provideFirebaseDatabaseRef(): DatabaseReference {
        return Firebase.database.reference
    }

    @Provides
    @Singleton
    fun provideGoalDataSource(
        firebaseAuth: FirebaseAuth,
        databaseReference: DatabaseReference
    ): GoalDataSource {
        return GoalDataSourceImpl(firebaseAuth, databaseReference)
    }
}