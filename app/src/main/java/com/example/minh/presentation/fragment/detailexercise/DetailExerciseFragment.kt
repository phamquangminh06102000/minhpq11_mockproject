package com.example.minh.presentation.fragment.detailexercise

import android.annotation.SuppressLint
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.navigation.fragment.navArgs
import com.example.minh.databinding.DetailExerciseScreenBinding
import com.example.minh.presentation.base.BaseFragment

class DetailExerciseFragment :
    BaseFragment<DetailExerciseScreenBinding>(DetailExerciseScreenBinding::inflate) {

    private val args by navArgs<DetailExerciseFragmentArgs>()

    @SuppressLint("SetJavaScriptEnabled")
    override fun bindView() {
        super.bindView()
        with(binding) {
            tvNameExercise.text = args.detailExercise?.name
            tvTypeExercise.text = args.detailExercise?.type
            tvMuscleExercise.text = args.detailExercise?.muscle
            tvDifficultyExercise.text = args.detailExercise?.difficulty
            tvInstructionExercise.text = args.detailExercise?.instructions
            videoExercise.settings.apply {
                javaScriptEnabled = true
                loadsImagesAutomatically = true
                cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
                layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
                setSupportZoom(true)
                mediaPlaybackRequiresUserGesture = false
            }
            videoExercise.webViewClient = WebViewClient()
            videoExercise.loadUrl(args.detailExercise!!.video)
        }
    }
}