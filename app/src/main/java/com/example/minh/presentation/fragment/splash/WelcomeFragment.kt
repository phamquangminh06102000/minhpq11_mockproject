package com.example.minh.presentation.fragment.splash

import android.view.View
import android.view.animation.AnimationUtils
import androidx.navigation.fragment.findNavController
import com.example.minh.R
import com.example.minh.databinding.WelcomeScreenBinding
import com.example.minh.presentation.base.BaseFragment
import com.example.minh.utils.options
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
@Suppress("UNUSED_EXPRESSION")
class WelcomeFragment : BaseFragment<WelcomeScreenBinding>(WelcomeScreenBinding::inflate),
    View.OnClickListener {

    override fun onClick(view: View?) {
        with(binding) {
            when (view) {
                btnNext -> findNavController().navigate(
                    R.id.action_welcomeFragment_to_registerFragment,
                    null,
                    options
                )
            }
        }
    }

    override fun bindView() {
        with(binding) {
            tvGoal.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide_in_left
                )
            )
            tvDetailGoal.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide_in_left
                )
            )
            tvGetBurn.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide_in_left
                )
            )
            tvDetailGetBurn.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide_in_left
                )
            )
            tvImprove.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide_in_left
                )
            )
            tvDetailImprove.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide_in_left
                )
            )
            btnNext.setOnClickListener(this@WelcomeFragment)
        }
    }
}