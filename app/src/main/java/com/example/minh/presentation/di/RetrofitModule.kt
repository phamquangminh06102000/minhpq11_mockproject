package com.example.minh.presentation.di

import com.example.minh.data.network.api.exerciseapi.ExerciseMockApi
import com.example.minh.data.network.api.exerciseapi.ExerciseMockApiDataSource
import com.example.minh.data.network.api.exerciseapi.ExerciseMockApiDataSourceImpl
import com.example.minh.data.network.api.exerciseimageapi.ExerciseImageMockApi
import com.example.minh.data.network.api.exerciseimageapi.ExerciseImageMockApiDataSource
import com.example.minh.data.network.api.exerciseimageapi.ExerciseImageMockApiDataSourceImpl
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.excludeFieldsWithoutExposeAnnotation()
        return gsonBuilder.create()
    }


    @Provides
    @Singleton
    fun providesHttpLoggingInterceptor() = HttpLoggingInterceptor()
        .apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Provides
    @Singleton
    fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient
            .Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit
        .Builder()
        .baseUrl("https://d541b64c-f12c-4efe-8630-921e6e348764.mock.pstmn.io/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun provideExerciseApi(retrofit: Retrofit): ExerciseMockApi =
        retrofit.create(ExerciseMockApi::class.java)


    @Provides
    @Singleton
    fun provideExerciseMockApiDatasource(
        exerciseMockApi: ExerciseMockApi,
    ): ExerciseMockApiDataSource = ExerciseMockApiDataSourceImpl(exerciseMockApi)

    @Provides
    @Singleton
    fun provideExerciseImageApi(retrofit: Retrofit): ExerciseImageMockApi =
        retrofit.create(ExerciseImageMockApi::class.java)

    @Provides
    @Singleton
    fun provideExerciseImageMockApiDatasource(
        exerciseImageMockApi: ExerciseImageMockApi
    ): ExerciseImageMockApiDataSource = ExerciseImageMockApiDataSourceImpl(exerciseImageMockApi)
}