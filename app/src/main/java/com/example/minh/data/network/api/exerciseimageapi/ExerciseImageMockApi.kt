package com.example.minh.data.network.api.exerciseimageapi

import com.example.minh.data.model.ExerciseImageApi
import retrofit2.http.GET

interface ExerciseImageMockApi {

    @GET("exercisesimage")
    suspend fun getExercisesImage(): List<ExerciseImageApi>
}