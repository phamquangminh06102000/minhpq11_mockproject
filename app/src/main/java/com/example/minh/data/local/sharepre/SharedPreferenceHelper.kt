package com.example.minh.data.local.sharepre

import android.content.SharedPreferences
import androidx.core.content.edit
import com.example.minh.utils.*
import javax.inject.Inject

class SharedPreferenceHelper @Inject constructor(
    private val sharedPreferences: SharedPreferences
){

    //put value: email
    fun setEmail(email: String){
        sharedPreferences.edit {
            putString(EMAIL, email)
            apply()
        }
    }

    //get value: email
    fun getEmail() = sharedPreferences.getString(EMAIL, "")

    //set value: password
    fun setPassword(password: String){
        sharedPreferences.edit {
            putString(PASSWORD, password)
            apply()
        }
    }

    //get value: password
    fun getPassword() = sharedPreferences.getString(PASSWORD, "")

    //set value: User name
    fun setUserName(userName: String){
        sharedPreferences.edit {
            putString(USER_NAME, userName)
            apply()
        }
    }

    //get value: User name
    fun getUSerName() = sharedPreferences.getString(USER_NAME, "")

    //set boolean: isRemember
    fun setIsRemember(isRemember: Boolean){
        sharedPreferences.edit {
            putBoolean(REMEMBER, isRemember)
            apply()
        }
    }


    fun setIsOpenFirstTime(isOpenFirstTime: Boolean) {
        sharedPreferences.edit {
            putBoolean(IS_OPEN_FIRST_TIME, isOpenFirstTime)
            apply()
        }
    }

    fun getIsOpenFirstTime() = sharedPreferences.getBoolean(IS_OPEN_FIRST_TIME, true)


    //get boolean
    fun getIsRemember() = sharedPreferences.getBoolean(REMEMBER, false)

    //set boolean: isLoggedIn
    fun setIsLoggedIn(isLoggedIn: Boolean) {
        sharedPreferences.edit {
            putBoolean(IS_LOGGED_IN, isLoggedIn)
            apply()
        }
    }

    //get boolean
    fun getIsLoggedIn() = sharedPreferences.getBoolean(IS_LOGGED_IN, false)

    fun getStepsCounted() = sharedPreferences.getInt(STEPS_COUNTED, 0)

    fun setStepsCounted(steps: Int) {
        sharedPreferences.edit {
            putInt(STEPS_COUNTED, steps)
            apply()
        }
    }

    fun getCurrentSteps() = sharedPreferences.getInt(CURRENT_STEP, 0)

    fun setCurrentSteps(steps: Int) {
        sharedPreferences.edit {
            putInt(CURRENT_STEP, steps)
            apply()
        }
    }


}