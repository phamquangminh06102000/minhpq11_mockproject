package com.example.minh.data.local.goal

import androidx.room.*
import com.example.minh.data.model.GoalEntity
import com.example.minh.domain.FireBaseState
import kotlinx.coroutines.flow.Flow

@Dao
interface GoalDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertToDatabase(goalEntity: List<GoalEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGoal(goalEntity: GoalEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateGoal(goalEntity: GoalEntity)

    @Delete
    suspend fun deleteGoal(goalEntity: GoalEntity)

    @Query("SELECT * FROM Goals")
    fun getAllGoals(): Flow<List<GoalEntity>>
}