package com.example.minh.data.model

data class ExerciseImageApi(
    val photo: String = ""
)