package com.example.minh.data.repository

import com.example.minh.data.mapper.ExerciseMapper
import com.example.minh.data.network.api.exerciseapi.ExerciseMockApiDataSource
import com.example.minh.domain.model.Exercise
import com.example.minh.domain.repository.ExerciseRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ExerciseRepositoryImpl(
    private val exerciseMockApiDataSource: ExerciseMockApiDataSource
): ExerciseRepository {
    override suspend fun getExercises(): Flow<List<Exercise>> {
        return exerciseMockApiDataSource.getExercises().map {
            ExerciseMapper.mapFromMockApi(it)
        }
    }
}