package com.example.minh.data.network.api.exerciseimageapi

import com.example.minh.data.model.ExerciseImageApi
import kotlinx.coroutines.flow.Flow

interface ExerciseImageMockApiDataSource {

    suspend fun getExercisesImage(): Flow<List<ExerciseImageApi>>
}