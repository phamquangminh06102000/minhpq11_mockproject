package com.example.minh.data.network.firebase.goal

import android.util.Log
import com.example.minh.data.model.GoalFirebase
import com.example.minh.utils.*
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

@Suppress("UNREACHABLE_CODE")
class GoalDataSourceImpl @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    private val referenceDatabase: DatabaseReference
) : GoalDataSource {

    override suspend fun addGoal(goalFirebase: GoalFirebase) {
        try {
            val goal = goalFirebase.copy(
                uid = firebaseAuth.currentUser!!.uid
            )
            val goalReference = referenceDatabase.child(GOAL).child(goal.uid!!).push()
            goal.goalId = goalReference.key
            goalReference.setValue(goal).await()

        } catch (ex: FirebaseException) {
        }
    }

    override suspend fun updateGoal(goalFirebase: GoalFirebase) {
        try {
            val goalReference = referenceDatabase.child(GOAL)
            goalReference.child(firebaseAuth.currentUser!!.uid)
                .child(goalFirebase.goalId.toString())

            goalReference.child(GOAL_NAME).setValue(goalFirebase.goalName)
            goalReference.child(GOAL_DES).setValue(goalFirebase.goalDes)
            goalReference.child(TARGET).setValue(goalFirebase.target)
            goalReference.child(DURATION).setValue(goalFirebase.duration)
            goalReference.child(STEP_PER_DAY).setValue(goalFirebase.stepsPerDay)
            goalReference.child(CALORIES_PER_DAY).setValue(goalFirebase.caloriesBurnPerDay)
            goalReference.child(TARGET_WEIGHT).setValue(goalFirebase.targetWeight)
            goalReference.child(TARGET_DATE).setValue(goalFirebase.targetDate)
        } catch (ex: FirebaseException) {
        }
    }

    override suspend fun deleteGoal(goalFirebase: GoalFirebase) {
        try {
            val goalReference = referenceDatabase.child(GOAL)
            goalReference.child(firebaseAuth.currentUser!!.uid)
                .child(goalFirebase.goalId.toString())
            goalReference.removeValue()
        } catch (ex: FirebaseException) {

        }
    }

    override fun getGoal(): Flow<List<GoalFirebase>> {
        val goalReference = referenceDatabase.child(GOAL).child(firebaseAuth.currentUser!!.uid)
        val goalUser = arrayListOf<GoalFirebase>()
        return callbackFlow {
            val postListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    goalUser.clear()
                    // Get Post object and use the values to update the UI
                    for (goal in dataSnapshot.children) {
                        goal.getValue(GoalFirebase::class.java)
                            ?.let {
                                goalUser.add(it)
                            }
                    }
                    trySend(goalUser)
                    Log.d("Minh", "onDataChange:  line = 92 $goalUser")
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    // Getting Post failed, log a message
                }
            }

            goalReference.addValueEventListener(postListener)

            awaitClose {
                goalReference.removeEventListener(postListener)
            }
        }
    }
}
