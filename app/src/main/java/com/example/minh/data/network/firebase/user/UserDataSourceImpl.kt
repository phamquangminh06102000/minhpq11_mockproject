package com.example.minh.data.network.firebase.user

import android.util.Log
import com.example.minh.data.model.UserFirebase
import com.example.minh.data.model.WorkoutLoggingFirebase
import com.example.minh.domain.FireBaseState
import com.example.minh.utils.USER
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await

class UserDataSourceImpl(
    private val firebaseAuth: FirebaseAuth,
    private val referenceDatabase: DatabaseReference
) : UserDataSource {

    override suspend fun register(userFirebase: UserFirebase): FireBaseState<String> {
        return try {
            firebaseAuth.createUserWithEmailAndPassword(
                userFirebase.email,
                userFirebase.password
            ).await()

            if (firebaseAuth.currentUser != null) {
                val userFirebaseRegister = UserFirebase(
                    uid = firebaseAuth.currentUser!!.uid,
                    userName = userFirebase.userName,
                    email = userFirebase.email,
                    password = userFirebase.password,
                    dateOfBirth = userFirebase.dateOfBirth,
                    height = userFirebase.height,
                    weight = userFirebase.weight,
                )

                val userReference = referenceDatabase.child(USER)
                userReference.child(userFirebaseRegister.uid).setValue(userFirebaseRegister).await()
                FireBaseState.Success("")
            } else {
                FireBaseState.Fail("", "")
            }
        } catch (ex: FirebaseException) {
            return FireBaseState.Fail(ex.message!!, "")
        }
    }

    override suspend fun registerBodyInformation(userFirebase: UserFirebase): FireBaseState<String> {
        return try {
            val userFirebaseRegister = UserFirebase(
                uid = firebaseAuth.currentUser!!.uid,
                userName = userFirebase.userName,
                email = userFirebase.email,
                password = userFirebase.password,
                dateOfBirth = userFirebase.dateOfBirth,
                height = userFirebase.height,
                weight = userFirebase.weight,
            )

            val userReference = referenceDatabase.child(USER)
            userReference.child(userFirebaseRegister.uid).setValue(userFirebaseRegister).await()

            firebaseAuth.signOut()

            FireBaseState.Success("")

        } catch (ex: FirebaseException) {
            return FireBaseState.Fail(ex.message!!, "")
        }
    }

    override suspend fun login(email: String, password: String): FireBaseState<String> {
        return try {
            firebaseAuth.signInWithEmailAndPassword(email, password).await()
            FireBaseState.Success("")
        } catch (ex: FirebaseException) {
            return FireBaseState.Fail(ex.message!!, "")
        }
    }


    override fun getCurrentUser(): Flow<UserFirebase> {
        val userReference =
            referenceDatabase.child(USER).child(firebaseAuth.currentUser!!.uid)

        return callbackFlow {
            val postListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // Get Post object and use the values to update the UI
                    val userFirebaseLogin = dataSnapshot.getValue(UserFirebase::class.java)
                    trySend(userFirebaseLogin!!)
                    Log.d("Minh", "onDataChange:  line = 72 $userFirebaseLogin")
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    // Getting Post failed, log a message
                }
            }

            userReference.addValueEventListener(postListener)

            awaitClose {
                userReference.removeEventListener(postListener)
            }
        }
    }

    override suspend fun updateLogging(workoutLoggingFirebase: WorkoutLoggingFirebase) {
        firebaseAuth.currentUser?.let {
            referenceDatabase.child("user").child(firebaseAuth.currentUser!!.uid)
                .child("WORKOUT_LOGGINGS").child(workoutLoggingFirebase.id)
                .setValue(workoutLoggingFirebase).await()
        }
    }

    override fun getWorkoutLogging(): Flow<List<WorkoutLoggingFirebase>> {

        val currentUserWorkoutLoggingsReference =
            referenceDatabase.child(USER).child(firebaseAuth.currentUser!!.uid).let { userId ->
                referenceDatabase.child(
                    "WORKOUT_LOGGINGS"
                )
            }

        return callbackFlow {
            val listener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val workoutLoggings = mutableListOf<WorkoutLoggingFirebase>()

                    for (workLogging in snapshot.children) {
                        workLogging.getValue(WorkoutLoggingFirebase::class.java)?.let {
                            workoutLoggings.add(it)
                        }
                    }

                    trySend(workoutLoggings)
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.d("ducna", "onCancelled: called")
                }
            }


            currentUserWorkoutLoggingsReference?.addValueEventListener(listener)
            awaitClose { currentUserWorkoutLoggingsReference?.removeEventListener(listener) }

        }
    }

    override fun logout() {
        firebaseAuth.signOut()
    }
}