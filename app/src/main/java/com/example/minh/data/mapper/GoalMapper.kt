package com.example.minh.data.mapper

import com.example.minh.data.model.GoalEntity
import com.example.minh.data.model.GoalFirebase
import com.example.minh.domain.model.Goal

object GoalMapper {

    fun mapFromDomainToFirebase(source: Goal): GoalFirebase {
        return GoalFirebase().apply {
            this.goalName = source.goalName!!
            this.goalDes = source.goalDes!!
            this.target = source.target!!
            this.duration = source.duration!!
            this.stepsPerDay = source.stepsPerDay!!
            this.targetWeight = source.targetWeight!!
            this.caloriesBurnPerDay = source.caloriesBurnPerDay!!
            this.targetDate = source.targetDate!!
        }
    }

    fun mapFromListEntityToDomain(source: List<GoalEntity>): List<Goal> {
        return source.map {
            Goal(
                goalName = it.goalName,
                goalDes = it.goalDes,
                target = it.target,
                duration = it.duration,
                goalId = it.goalId,
                stepsPerDay = it.stepsPerDay,
                targetWeight = it.targetWeight,
                caloriesBurnPerDay = it.caloriesBurnPerDay,
                targetDate = it.targetDate
            )
        }
    }

    fun mapFromListDomainToEntity(source: List<Goal>): List<GoalEntity> {
        return source.map {
            GoalEntity(
                goalName = it.goalName,
                goalDes = it.goalDes,
                target = it.target,
                duration = it.duration,
                goalId = it.goalId!!,
                stepsPerDay = it.stepsPerDay,
                targetWeight = it.targetWeight,
                caloriesBurnPerDay = it.caloriesBurnPerDay,
                targetDate = it.targetDate
            )
        }
    }

    fun mapFromListFirebaseToDomain(source: List<GoalFirebase>): List<Goal> {
        return source.map {
            Goal(
                goalId = it.goalId,
                goalName = it.goalName,
                goalDes = it.goalDes,
                target = it.target,
                duration = it.duration,
                stepsPerDay = it.stepsPerDay,
                targetWeight = it.targetWeight,
                caloriesBurnPerDay = it.caloriesBurnPerDay,
                targetDate = it.targetDate
            )
        }
    }

    fun mapFromDomainToEntity(source: Goal): GoalEntity {
        return GoalEntity(
            goalName = source.goalName,
            goalDes = source.goalDes,
            target = source.target,
            duration = source.duration,
            goalId = source.goalId!!,
            stepsPerDay = source.stepsPerDay!!,
            targetWeight = source.targetWeight!!,
            caloriesBurnPerDay = source.caloriesBurnPerDay!!,
            targetDate = source.targetDate!!
        )
    }
}