package com.example.minh.data.local.stepcounter

abstract class MeasurableSensor {

    protected var onSensorValuesChanged: OnSetValue? = null

    abstract val isSensorExist: Boolean

    abstract fun startListening()

    abstract fun stopListening()

    fun setOnSensorValuesChangedListener(listener: OnSetValue) {
        onSensorValuesChanged = listener
    }
}

interface OnSetValue {
    fun onValueChanged(value: List<Float>)
}