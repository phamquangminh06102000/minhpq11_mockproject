package com.example.minh.data.model

data class ExerciseApi(
    val name: String = "",
    val type: String = "",
    val muscle: String = "",
    val equipment: String,
    val difficulty: String,
    val photo: String = "",
    val video: String = "",
    val instructions: String
)