package com.example.minh.data.repository

import com.example.minh.data.local.stepcounter.MeasurableSensor
import com.example.minh.data.local.stepcounter.OnSetValue
import com.example.minh.domain.repository.SensorRepository
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class SensorRepositoryImpl(
    private val measurableSensor: MeasurableSensor
) : SensorRepository {

    override fun getSensorValues(): Flow<List<Float>> = callbackFlow {
        measurableSensor.startListening()

        val listener = object : OnSetValue {
            override fun onValueChanged(value: List<Float>) {
                trySend(value)
            }
        }

        measurableSensor.setOnSensorValuesChangedListener(listener)
        awaitClose {
            measurableSensor.stopListening()
        }
    }
}