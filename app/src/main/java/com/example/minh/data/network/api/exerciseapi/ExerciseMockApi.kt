package com.example.minh.data.network.api.exerciseapi

import com.example.minh.data.model.ExerciseApi
import retrofit2.http.GET

interface ExerciseMockApi {

    @GET("exercises")
    suspend fun getExercises(): List<ExerciseApi>
}