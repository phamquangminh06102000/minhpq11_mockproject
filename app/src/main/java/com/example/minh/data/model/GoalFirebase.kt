package com.example.minh.data.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class GoalFirebase(
    var uid: String? = null,
    var goalId: String? = null,
    var goalName: String? = null,
    var goalDes: String? = null,
    var target: String? = null,
    var duration: String? = null,
    var stepsPerDay: Int? = null,
    var targetWeight: Double? = null,
    var caloriesBurnPerDay: Double? = null,
    var targetDate: String? = null
)