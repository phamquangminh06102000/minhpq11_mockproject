package com.example.minh.data.network.firebase.user

import com.example.minh.data.model.UserFirebase
import com.example.minh.data.model.WorkoutLoggingFirebase
import com.example.minh.domain.FireBaseState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector

interface UserDataSource {
    //register user
    suspend fun register(userFirebase: UserFirebase): FireBaseState<String>

    //register information
    suspend fun registerBodyInformation(userFirebase: UserFirebase): FireBaseState<String>

    //login
    suspend fun login(email: String, password: String): FireBaseState<String>

    //getUser
    fun getCurrentUser(): Flow<UserFirebase>

    //logging
    suspend fun updateLogging(workoutLoggingFirebase: WorkoutLoggingFirebase)

    //getLogging
    fun getWorkoutLogging(): Flow<List<WorkoutLoggingFirebase>>

    //logout
    fun logout()

}