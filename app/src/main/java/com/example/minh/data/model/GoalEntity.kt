package com.example.minh.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
@Entity(tableName = "Goals")
data class GoalEntity(

    @PrimaryKey
    var goalId: String,

    @ColumnInfo(name = "goalName")
    var goalName: String?,

    @ColumnInfo(name = "goalDes")
    var goalDes: String?,

    @ColumnInfo(name = "target")
    var target: String?,

    @ColumnInfo(name = "duration")
    var duration: String?,

    @ColumnInfo(name = "stepPerDay")
    var stepsPerDay: Int?,

    @ColumnInfo(name = "targetWeight")
    var targetWeight: Double?,

    @ColumnInfo(name = "calories")
    var caloriesBurnPerDay: Double?,

    @ColumnInfo(name = "date")
    var targetDate: String?
)
