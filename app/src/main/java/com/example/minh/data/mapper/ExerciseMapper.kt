package com.example.minh.data.mapper

import com.example.minh.data.model.ExerciseApi
import com.example.minh.domain.model.Exercise

object ExerciseMapper {

    fun mapFromMockApi(source: List<ExerciseApi>): List<Exercise> {
        return source.map {
            Exercise(
                name = it.name,
                type = it.type,
                muscle = it.muscle,
                equipment = it.equipment,
                difficulty = it.difficulty,
                photo = it.photo,
                video = it.video,
                instructions = it.instructions
            )
        }
    }

    fun mapFromDomainModel(source: List<Exercise>): List<ExerciseApi> {
        return source.map {
            ExerciseApi(
                name = it.name,
                type = it.type,
                muscle = it.muscle,
                equipment = it.equipment,
                difficulty = it.difficulty,
                photo = it.photo,
                video = it.video,
                instructions = it.instructions
            )
        }
    }
}