package com.example.minh.data.repository

import com.example.minh.data.mapper.UserMapper
import com.example.minh.data.mapper.WorkoutLoggingMapper
import com.example.minh.data.network.firebase.user.UserDataSource
import com.example.minh.domain.FireBaseState
import com.example.minh.domain.model.User
import com.example.minh.domain.model.WorkoutLogging
import com.example.minh.domain.repository.UserRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class UserRepositoryImpl(
    private val userDataSource: UserDataSource,
) : UserRepository {

    override suspend fun login(email: String, password: String): FireBaseState<String> {
        return userDataSource.login(email, password)
    }

    override suspend fun register(user: User): FireBaseState<String> {
        return userDataSource.register(UserMapper.mapFromDomainModel(user))
    }

    override suspend fun bodyInformation(user: User): FireBaseState<String> {
        return userDataSource.registerBodyInformation(UserMapper.mapFromDomainModel(user))
    }


    override fun getCurrentUser(): Flow<User> {
        return userDataSource.getCurrentUser().map {
            UserMapper.mapFromFirebaseModel(it)
        }
    }

    override fun getWorkoutLogging(): Flow<List<WorkoutLogging>> {
        return userDataSource.getWorkoutLogging().map {
            WorkoutLoggingMapper.mapFromListWorkoutLoggingFirebaseToDomain(it)
        }
    }

    override fun logout() = userDataSource.logout()

    override suspend fun updateLogging(workoutLogging: WorkoutLogging) {
        return userDataSource.updateLogging(
            WorkoutLoggingMapper.mapFromWorkoutLoggingDomainToFirebase(
                workoutLogging
            )
        )
    }
}