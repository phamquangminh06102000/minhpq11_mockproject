package com.example.minh.data.network.api.exerciseapi

import com.example.minh.data.model.ExerciseApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class ExerciseMockApiDataSourceImpl(
    private val exerciseMockApi: ExerciseMockApi
) : ExerciseMockApiDataSource {

    override suspend fun getExercises(): Flow<List<ExerciseApi>> {
        return flow {
            emit(exerciseMockApi.getExercises())
        }.flowOn(Dispatchers.IO)
    }
}