package com.example.minh.data.network.firebase.goal

import com.example.minh.data.model.GoalFirebase
import com.example.minh.domain.FireBaseState
import kotlinx.coroutines.flow.Flow

interface GoalDataSource {

    suspend fun addGoal(goalFirebase: GoalFirebase)

    suspend fun updateGoal(goalFirebase: GoalFirebase)

    suspend fun deleteGoal(goalFirebase: GoalFirebase)

    fun getGoal(): Flow<List<GoalFirebase>>
}