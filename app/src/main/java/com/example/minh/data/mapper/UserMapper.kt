package com.example.minh.data.mapper

import com.example.minh.data.model.UserFirebase
import com.example.minh.domain.model.User

object UserMapper {

    fun mapFromFirebaseModel(source: UserFirebase): User {
        return User(
            userName = source.userName,
            email = source.email,
            password = source.password,
            dateOfBirth = source.dateOfBirth,
            height = source.height,
            weight = source.weight,
            loggings = source.loggings?.map {
                WorkoutLoggingMapper.mapFromWorkoutLoggingFirebaseToDomain(it)
            }
        )
    }

    fun mapFromDomainModel(source: User): UserFirebase {
        return UserFirebase(
            userName = source.userName,
            email = source.email,
            password = source.password,
            dateOfBirth = source.dateOfBirth,
            height = source.height,
            weight = source.weight,
            loggings = source.loggings?.map {
                WorkoutLoggingMapper.mapFromWorkoutLoggingDomainToFirebase(it)
            }
        )
    }
}
