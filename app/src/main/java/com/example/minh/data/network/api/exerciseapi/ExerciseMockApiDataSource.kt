package com.example.minh.data.network.api.exerciseapi

import com.example.minh.data.model.ExerciseApi
import kotlinx.coroutines.flow.Flow

interface ExerciseMockApiDataSource {

    suspend fun getExercises(): Flow<List<ExerciseApi>>
}