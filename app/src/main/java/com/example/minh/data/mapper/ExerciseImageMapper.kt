package com.example.minh.data.mapper

import com.example.minh.data.model.ExerciseImageApi
import com.example.minh.domain.model.ExerciseImage

object ExerciseImageMapper {

    fun mapFromListDataSourceToDomain(source: List<ExerciseImageApi>): List<ExerciseImage> {
        return source.map {
            ExerciseImage(photo = it.photo)
        }
    }
}