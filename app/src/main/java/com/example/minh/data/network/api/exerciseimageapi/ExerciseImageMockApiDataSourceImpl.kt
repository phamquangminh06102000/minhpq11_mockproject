package com.example.minh.data.network.api.exerciseimageapi

import com.example.minh.data.model.ExerciseImageApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class ExerciseImageMockApiDataSourceImpl(
    private val exerciseImageMockApi: ExerciseImageMockApi
) : ExerciseImageMockApiDataSource {
    override suspend fun getExercisesImage(): Flow<List<ExerciseImageApi>> {
        return flow {
            emit(exerciseImageMockApi.getExercisesImage())
        }.flowOn(Dispatchers.IO)
    }
}