package com.example.minh.data.repository

import com.example.minh.data.mapper.GoalMapper
import com.example.minh.data.network.firebase.goal.GoalDataSource
import com.example.minh.domain.model.Goal
import com.example.minh.domain.repository.GoalRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GoalRepositoryFirebaseImpl(
    private val goalDataSource: GoalDataSource
) : GoalRepository {

    override suspend fun savaToDatabase(goalList: List<Goal>) {
        TODO("Not yet implemented")
    }

    override suspend fun insertGoal(goal: Goal){
        return goalDataSource.addGoal(GoalMapper.mapFromDomainToFirebase(goal))
    }

    override suspend fun updateGoal(goal: Goal){
        return goalDataSource.updateGoal(GoalMapper.mapFromDomainToFirebase(goal))
    }

    override suspend fun deleteGoal(goal: Goal) {
        return goalDataSource.deleteGoal(GoalMapper.mapFromDomainToFirebase(goal))
    }

    override fun getAllGoals(): Flow<List<Goal>> {
        return goalDataSource.getGoal().map {
            GoalMapper.mapFromListFirebaseToDomain(it)
        }
    }
}