package com.example.minh.data.local.goal

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.minh.data.model.GoalEntity


@Database(entities = [GoalEntity::class], version = 5)
abstract class GoalDatabase : RoomDatabase() {
    abstract fun getGoalDao(): GoalDao
}