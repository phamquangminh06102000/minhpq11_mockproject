package com.example.minh.data.repository

import android.util.Log
import com.example.minh.data.local.goal.GoalDao
import com.example.minh.data.mapper.GoalMapper
import com.example.minh.domain.FireBaseState
import com.example.minh.domain.model.Goal
import com.example.minh.domain.repository.GoalRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GoalRepositoryLocalImpl(
    private val goalDao: GoalDao
) : GoalRepository {

    override suspend fun savaToDatabase(goalList: List<Goal>) {
        Log.d("Minh", "savaToDatabase:  line = 16 ${GoalMapper.mapFromListDomainToEntity(goalList)}")
        return goalDao.insertToDatabase(GoalMapper.mapFromListDomainToEntity(goalList))

    }

    override suspend fun insertGoal(goal: Goal) {
        return goalDao.insertGoal(GoalMapper.mapFromDomainToEntity(goal))
    }

    override suspend fun updateGoal(goal: Goal) {
        return goalDao.updateGoal(GoalMapper.mapFromDomainToEntity(goal))
    }

    override suspend fun deleteGoal(goal: Goal) {
        return goalDao.deleteGoal(GoalMapper.mapFromDomainToEntity(goal))
    }

    override fun getAllGoals(): Flow<List<Goal>> {
        return goalDao.getAllGoals().map {
            GoalMapper.mapFromListEntityToDomain(it)
        }
    }
}