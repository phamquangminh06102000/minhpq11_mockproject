package com.example.minh.data.mapper

import com.example.minh.data.model.WorkoutLoggingFirebase
import com.example.minh.domain.model.WorkoutLogging

object WorkoutLoggingMapper {
    fun mapFromWorkoutLoggingFirebaseToDomain(source: WorkoutLoggingFirebase): WorkoutLogging {
        return WorkoutLogging(
            id = source.id,
            currentStep = source.currentStep,
            currentCalories = source.currentCalories,
            date = source.date,
            height = source.height,
            weight = source.weight
        )
    }

    fun mapFromWorkoutLoggingDomainToFirebase(source: WorkoutLogging): WorkoutLoggingFirebase {
        return WorkoutLoggingFirebase(
            id = source.id,
            currentStep = source.currentStep,
            currentCalories = source.currentCalories,
            date = source.date,
            height = source.height,
            weight = source.weight
        )
    }

    fun mapFromListWorkoutLoggingFirebaseToDomain(source: List<WorkoutLoggingFirebase>): List<WorkoutLogging> {
        return source.map {
            WorkoutLogging(
                id = it.id,
                currentStep = it.currentStep,
                currentCalories = it.currentCalories,
                date = it.date,
                height = it.height,
                weight = it.weight
            )
        }
    }

    fun mapFromListWorkoutLoggingDomainToFirebase(source: List<WorkoutLogging>): List<WorkoutLoggingFirebase> {
        return source.map {
            WorkoutLoggingFirebase(
                id = it.id,
                currentStep = it.currentStep,
                currentCalories = it.currentCalories,
                date = it.date,
                height = it.height,
                weight = it.weight
            )
        }
    }
}