package com.example.minh.data.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class UserFirebase(

    val uid: String = "",
    val userName: String = "",
    val email: String = "",
    val password: String = "",
    val dateOfBirth: String? = null,
    val height: String? = null,
    val weight: String? = null,
    var loggings: List<WorkoutLoggingFirebase>? = null
)
