package com.example.minh.data.repository

import com.example.minh.data.mapper.ExerciseImageMapper
import com.example.minh.data.network.api.exerciseimageapi.ExerciseImageMockApiDataSource
import com.example.minh.domain.model.ExerciseImage
import com.example.minh.domain.repository.ExerciseImageRepositpry
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ExerciseImageRepositoryImpl(
    private val exerciseImageMockApiDataSource: ExerciseImageMockApiDataSource
) : ExerciseImageRepositpry {

    override suspend fun getExercisesImage(): Flow<List<ExerciseImage>> {
        return exerciseImageMockApiDataSource.getExercisesImage().map {
            ExerciseImageMapper.mapFromListDataSourceToDomain(it)
        }
    }
}