package com.example.minh.utils

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.example.minh.presentation.WorkoutLoggingReceiver
import dagger.hilt.android.HiltAndroidApp
import java.util.*

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        setUpdateLoggings()
    }

    private fun setUpdateLoggings() {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val intent = Intent(this, WorkoutLoggingReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            this,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )

        val time = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, 14)
            set(Calendar.MINUTE, 39)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }

//        if (Calendar.getInstance().after(time)) {
//            time.add(Calendar.DAY_OF_MONTH, 1);
//        }

        alarmManager.setRepeating(
            AlarmManager.RTC_WAKEUP,
            time.timeInMillis,
            1 * 60 * 1000,
            pendingIntent
        )
    }
}

