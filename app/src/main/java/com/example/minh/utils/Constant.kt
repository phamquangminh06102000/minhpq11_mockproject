package com.example.minh.utils

import androidx.navigation.navOptions
import com.example.minh.R

val options = navOptions {
    anim {
        enter = R.anim.slide_in_left
        exit = R.anim.slide_out_right
    }
}

const val TAG = "MinhPQ11"
const val MY_SHARED_PREFERENCE = "My Shared Preference"
const val EMAIL = "USER_EMAIL"
const val PASSWORD = "USER_PASSWORD"
const val USER_NAME = "USER_NAME"
const val REMEMBER = "IS_REMEMBER"
const val IS_LOGGED_IN = "IS_LOGGED_IN"
const val IS_OPEN_FIRST_TIME = "IS_OPEN_FIRST_TIME"

const val TYPE_USER_NAME = "typeUserName"
const val TYPE_EMAIL = "typeEmail"
const val TYPE_PASSWORD = "typePassword"
const val TYPE_DATEOFBIRTH = "typeDateOfBirth"
const val TYPE_HEIGHT = "typeHeight"
const val TYPE_WEIGHT = "typeWeight"
const val STEP_PER_DAY = "STEP_PER_DAY"
const val CALORIES_PER_DAY = "CALORIES_PER_DAY"
const val TARGET_WEIGHT = "TARGET_WEIGHT"
const val TARGET_DATE = "TARGET_DATE"

const val GOAL_NAME = "goalName"
const val GOAL_DES = "goalDes"
const val TARGET = "goalTarget"
const val DURATION = "goalDuration"

const val GOAL = "Goal"
const val USER = "user"

const val STEPS_COUNTED = "STEPS_COUNTED"
const val CURRENT_STEP = "CURRENT_STEP"