package com.example.minh.utils

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions

fun NavController.safeNavigate(direction: NavDirections, aim: NavOptions) {
    currentDestination?.getAction(direction.actionId)?.run {
        navigate(direction, aim)
    }
}


