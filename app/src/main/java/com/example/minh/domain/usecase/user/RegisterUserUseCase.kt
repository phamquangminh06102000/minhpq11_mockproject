package com.example.minh.domain.usecase.user

import com.example.minh.data.local.sharepre.SharedPreferenceHelper
import com.example.minh.domain.FireBaseState
import com.example.minh.domain.model.User
import com.example.minh.domain.repository.UserRepository
import com.example.minh.utils.TYPE_EMAIL
import com.example.minh.utils.TYPE_PASSWORD
import com.example.minh.utils.TYPE_USER_NAME
import com.example.minh.utils.Validator
import javax.inject.Inject

class RegisterUserUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {

    suspend operator fun invoke(
        userName: String, email: String, password: String
    ): FireBaseState<String> {
        return when {
            userName.isEmpty() -> FireBaseState.Fail("Name can't be empty", TYPE_USER_NAME)
            email.isEmpty() -> FireBaseState.Fail("Email can't be empty", TYPE_EMAIL)
            password.isEmpty() -> FireBaseState.Fail("Password can't be empty", TYPE_PASSWORD)
            !Validator.isValidName(userName) -> FireBaseState.Fail(
                "User Name is inValid",
                TYPE_USER_NAME
            )
            !Validator.isValidEmail(email) -> FireBaseState.Fail("Email is inValid ", TYPE_EMAIL)
            !Validator.isValidPassword(password) -> FireBaseState.Fail(
                "Password is inValid",
                TYPE_PASSWORD
            )
            else -> {
                val user = User(userName = userName, email = email, password = password)
                val responseState: FireBaseState<String> = userRepository.register(user)

                if (responseState is FireBaseState.Success) {
                    sharedPreferenceHelper.setEmail(email)
                    sharedPreferenceHelper.setPassword(password)
                    sharedPreferenceHelper.setUserName(userName)
                }
                return responseState
            }
        }
    }
}