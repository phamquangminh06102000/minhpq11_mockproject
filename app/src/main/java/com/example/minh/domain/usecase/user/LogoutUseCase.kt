package com.example.minh.domain.usecase.user

import com.example.minh.data.local.sharepre.SharedPreferenceHelper
import com.example.minh.domain.repository.UserRepository
import javax.inject.Inject

class LogoutUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {
    operator fun invoke() {
        userRepository.logout()
        sharedPreferenceHelper.setIsLoggedIn(false)
    }
}