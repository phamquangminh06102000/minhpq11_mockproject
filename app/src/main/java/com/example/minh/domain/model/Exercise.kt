package com.example.minh.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Exercise(
    val name: String = "",
    val type: String = "",
    val muscle: String = "",
    val equipment: String,
    val difficulty: String,
    val photo: String = "",
    val video: String = "",
    val instructions: String
) : Parcelable