package com.example.minh.domain.usecase.goal

import com.example.minh.domain.model.Goal
import com.example.minh.domain.repository.GoalRepository
import com.example.minh.presentation.di.RepositoryModule
import javax.inject.Inject

class DeleteGoalUseCase @Inject constructor(
    @RepositoryModule.GoalRepositoryLocal private val goalRepositoryLocal: GoalRepository,
    @RepositoryModule.GoalRepositoryFirebase private val goalRepositoryFirebase: GoalRepository
) {
    suspend fun fromLocal(goal: Goal) = goalRepositoryLocal.deleteGoal(goal)
    suspend fun fromFirebase(goal: Goal) = goalRepositoryFirebase.deleteGoal(goal)
}