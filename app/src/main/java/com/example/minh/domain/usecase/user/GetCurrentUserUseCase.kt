package com.example.minh.domain.usecase.user

import com.example.minh.domain.model.User
import com.example.minh.domain.repository.UserRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCurrentUserUseCase @Inject constructor(private val userRepository: UserRepository) {
    operator fun invoke(): Flow<User> = userRepository.getCurrentUser()
}