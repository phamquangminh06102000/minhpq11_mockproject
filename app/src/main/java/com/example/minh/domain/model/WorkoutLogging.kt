package com.example.minh.domain.model

import kotlin.random.Random

data class WorkoutLogging(
    val id: String = System.currentTimeMillis().toString(),
    val currentStep: Int = 0,
    val currentCalories: Double = 0.0,
    val date: String = "",
    val height: Double = 0.0,
    val weight: Double = 0.0
) {

    companion object {
        fun randomWorkoutLogging() =
            WorkoutLogging(
                currentStep = Random.nextInt(1, 400),
                currentCalories = Random.nextDouble(1.toDouble(), 2000.toDouble()),
                date = listOf("Sun", "Mon", "Tues", "Thurs", "Fri", "Sat", "Sun").random(),
                height = Random.nextDouble(),
                weight = Random.nextDouble(),
            )
    }

}
