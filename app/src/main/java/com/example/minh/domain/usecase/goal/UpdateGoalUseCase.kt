package com.example.minh.domain.usecase.goal

import com.example.minh.domain.model.Goal
import com.example.minh.domain.repository.GoalRepository
import com.example.minh.presentation.di.RepositoryModule
import javax.inject.Inject

class UpdateGoalUseCase @Inject constructor(
    @RepositoryModule.GoalRepositoryLocal private val goalRepositoryLocal: GoalRepository,
    @RepositoryModule.GoalRepositoryFirebase private val goalRepositoryFirebase: GoalRepository
) {
    suspend fun fromLocal(goal: Goal) = goalRepositoryLocal.updateGoal(goal)
    suspend fun fromFirebase(goal: Goal) = goalRepositoryFirebase.updateGoal(goal)
}