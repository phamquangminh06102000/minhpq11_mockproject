package com.example.minh.domain.usecase.stepcounter

import com.example.minh.domain.repository.SensorRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetStepCounterUseCase @Inject constructor(
    private val sensorRepository: SensorRepository
) {
    operator fun invoke(): Flow<List<Float>> = sensorRepository.getSensorValues()
}