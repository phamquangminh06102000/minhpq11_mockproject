package com.example.minh.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlin.random.Random

@Parcelize
data class Goal(
    var goalId: String? = "",
    var goalName: String? = "",
    var goalDes: String? = "",
    var target: String? = "",
    var duration: String? = "",
    var stepsPerDay: Int? = 0,
    var targetWeight: Double? = 0.0,
    var caloriesBurnPerDay: Double? = 0.0,
    var targetDate: String? = ""
) : Parcelable {
    companion object {
        fun randomGoal() = Goal(
            stepsPerDay = 500,
            targetWeight = Random.nextDouble(),
            caloriesBurnPerDay = 2500.toDouble(),
        )
    }
}
