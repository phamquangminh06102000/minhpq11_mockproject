package com.example.minh.domain

sealed class FireBaseState<T>(
    val data: T? = null,
    val msg: String? = null,
    val type: String? = null
) {
    class Success<T>(data: T?) : FireBaseState<T>(data, null)
    class Fail<T>(msg: String?,type: String?) : FireBaseState<T>(null, msg, type)
    class Loading<T>(msg: String?) : FireBaseState<T>(null, msg)
}
