package com.example.minh.domain.model

data class ExerciseImage(
    val photo: String = ""
)
