package com.example.minh.domain.usecase.user

import com.example.minh.data.local.sharepre.SharedPreferenceHelper
import com.example.minh.domain.FireBaseState
import com.example.minh.domain.model.User
import com.example.minh.domain.repository.UserRepository
import com.example.minh.utils.TYPE_DATEOFBIRTH
import com.example.minh.utils.TYPE_HEIGHT
import com.example.minh.utils.TYPE_WEIGHT
import javax.inject.Inject

class RegisterBodyInformationUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {
    suspend operator fun invoke(
        dateOfBirth: String,
        height: String,
        weight: String
    ): FireBaseState<String> {
        return when {
            dateOfBirth.isEmpty() -> FireBaseState.Fail("Birthday is\'n empty", TYPE_DATEOFBIRTH)
            height.isEmpty() -> FireBaseState.Fail("Height is\'n empty", TYPE_HEIGHT)
            weight.isEmpty() -> FireBaseState.Fail("Weight is\'n empty", TYPE_WEIGHT)
            else -> {
                val user = User(dateOfBirth = dateOfBirth, height = height, weight = weight)
                if (user.email.isEmpty()) {
                    user.email = sharedPreferenceHelper.getEmail()!!
                }
                if (user.password.isEmpty()) {
                    user.password = sharedPreferenceHelper.getPassword()!!
                }
                if (user.userName.isEmpty()) {
                    user.userName = sharedPreferenceHelper.getUSerName()!!
                }
                return userRepository.bodyInformation(user)
            }
        }
    }
}