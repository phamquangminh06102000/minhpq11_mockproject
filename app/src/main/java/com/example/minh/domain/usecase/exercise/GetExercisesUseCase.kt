package com.example.minh.domain.usecase.exercise

import com.example.minh.domain.model.Exercise
import com.example.minh.domain.repository.ExerciseRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetExercisesUseCase @Inject constructor(
    private val exerciseRepository: ExerciseRepository
) {
    suspend operator fun invoke(): Flow<List<Exercise>> = exerciseRepository.getExercises()
}