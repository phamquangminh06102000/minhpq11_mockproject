package com.example.minh.domain.repository

import com.example.minh.domain.model.Goal
import kotlinx.coroutines.flow.Flow

interface GoalRepository {

    suspend fun savaToDatabase(goalList: List<Goal>)

    suspend fun insertGoal(goal: Goal)

    suspend fun updateGoal(goal: Goal)

    suspend fun deleteGoal(goal: Goal)

    fun getAllGoals(): Flow<List<Goal>>
}