package com.example.minh.domain.repository

import kotlinx.coroutines.flow.Flow

interface SensorRepository {

    fun getSensorValues() : Flow<List<Float>>
}