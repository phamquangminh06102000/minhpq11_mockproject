package com.example.minh.domain.usecase.goal

import com.example.minh.domain.model.Goal
import com.example.minh.domain.repository.GoalRepository
import com.example.minh.presentation.di.RepositoryModule
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetGoalUseCase @Inject constructor(
    @RepositoryModule.GoalRepositoryLocal private val goalRepositoryLocal: GoalRepository,
    @RepositoryModule.GoalRepositoryFirebase private val goalRepositoryFirebase: GoalRepository
) {
    fun fromLocal(): Flow<List<Goal>> = goalRepositoryLocal.getAllGoals()
    fun fromFirebase(): Flow<List<Goal>> = goalRepositoryFirebase.getAllGoals()
}