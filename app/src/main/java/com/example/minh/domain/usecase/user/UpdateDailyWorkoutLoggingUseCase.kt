package com.example.minh.domain.usecase.user

import android.util.Log
import com.example.minh.data.local.sharepre.SharedPreferenceHelper
import com.example.minh.domain.model.WorkoutLogging
import com.example.minh.domain.repository.UserRepository
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class UpdateDailyWorkoutLoggingUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {

    suspend operator fun invoke(): WorkoutLogging? {

        return if (sharedPreferenceHelper.getIsLoggedIn()) {
            val currentSteps = sharedPreferenceHelper.getCurrentSteps()
            sharedPreferenceHelper.run {
                setCurrentSteps(0)
                setStepsCounted(sharedPreferenceHelper.getStepsCounted() + currentSteps)
            }

            val id = System.currentTimeMillis()

            val currentWeekDay = SimpleDateFormat("EEE", Locale.US).format(Date(id))

            val workoutLogging = WorkoutLogging(
                id = id.toString(),
                currentStep = currentSteps,
                currentCalories = currentSteps * 0.04,
                date = currentWeekDay,
                height = 0.0,
                weight = 0.0,
            )

            userRepository.updateLogging(workoutLogging)

            Log.d("Minh", "invoke:  line = 39")

            workoutLogging

        } else {
            null
        }
    }

    private fun calories(stepCurrent: Int, weight: Double, height: Double, age: Int): Double {
        val bmr = 10 * height + 6.25 * weight - 5 * age + 5
        if (stepCurrent in 0 until 1000) {
            return bmr * 1.2
        }

        if (stepCurrent in 1000 until 2000) {
            return bmr * 1.375
        }
        if (stepCurrent in 2000 until 3000) {
            return bmr * 1.55
        }
        return if (stepCurrent in 3000 until 4000) {
            bmr * 1.75
        } else {
            bmr * 1.9
        }
    }
}




