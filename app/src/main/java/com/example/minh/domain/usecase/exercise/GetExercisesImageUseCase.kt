package com.example.minh.domain.usecase.exercise

import com.example.minh.domain.model.ExerciseImage
import com.example.minh.domain.repository.ExerciseImageRepositpry
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetExercisesImageUseCase @Inject constructor(
    private val exerciseImageRepository: ExerciseImageRepositpry
) {
    suspend operator fun invoke(): Flow<List<ExerciseImage>> =
        exerciseImageRepository.getExercisesImage()
}