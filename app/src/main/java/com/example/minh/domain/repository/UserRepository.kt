package com.example.minh.domain.repository

import com.example.minh.domain.FireBaseState
import com.example.minh.domain.model.User
import com.example.minh.domain.model.WorkoutLogging
import kotlinx.coroutines.flow.Flow

interface UserRepository {

    //login
    suspend fun login(email: String, password: String): FireBaseState<String>

    //register
    suspend fun register(user: User): FireBaseState<String>

    //register body information
    suspend fun bodyInformation(user: User): FireBaseState<String>

    //get current user
    fun getCurrentUser(): Flow<User>

    fun getWorkoutLogging(): Flow<List<WorkoutLogging>>

    fun logout()

    suspend fun updateLogging(workoutLogging: WorkoutLogging)

}