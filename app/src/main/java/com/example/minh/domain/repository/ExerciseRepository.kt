package com.example.minh.domain.repository

import com.example.minh.domain.model.Exercise
import kotlinx.coroutines.flow.Flow

interface ExerciseRepository {
    suspend fun getExercises(): Flow<List<Exercise>>
}