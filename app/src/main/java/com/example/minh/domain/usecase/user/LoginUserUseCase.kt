package com.example.minh.domain.usecase.user

import com.example.minh.data.local.sharepre.SharedPreferenceHelper
import com.example.minh.domain.FireBaseState
import com.example.minh.domain.repository.UserRepository
import com.example.minh.utils.TYPE_EMAIL
import com.example.minh.utils.TYPE_PASSWORD
import javax.inject.Inject

class LoginUserUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {

    suspend operator fun invoke(email: String, password: String): FireBaseState<String> {
        return when {
            email.isEmpty() -> FireBaseState.Fail("Email can't be empty", TYPE_EMAIL)
            password.isEmpty() -> FireBaseState.Fail("Password can't be empty", TYPE_PASSWORD)
            else -> {
                val responseState = userRepository.login(email, password)
                if (responseState is FireBaseState.Success) {
                    sharedPreferenceHelper.setEmail(email)
                    sharedPreferenceHelper.setPassword(password)
                    sharedPreferenceHelper.setIsLoggedIn(true)
                    sharedPreferenceHelper.setIsRemember(true)
                    sharedPreferenceHelper.setIsOpenFirstTime(false)
                }
                return responseState
            }
        }
    }
}