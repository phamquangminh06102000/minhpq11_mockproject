package com.example.minh.domain.model

data class User(
    var userName: String = "",
    var email: String = "",
    var password: String = "",
    var dateOfBirth: String? = null,
    var height: String? = null,
    var weight: String? = null,
    var loggings: List<WorkoutLogging>? = null
)