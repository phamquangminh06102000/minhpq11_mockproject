package com.example.minh.domain.repository

import com.example.minh.domain.model.ExerciseImage
import kotlinx.coroutines.flow.Flow

interface ExerciseImageRepositpry {

    suspend fun getExercisesImage(): Flow<List<ExerciseImage>>
}