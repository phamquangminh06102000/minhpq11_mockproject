package com.example.minh.domain.usecase.goal

import com.example.minh.domain.model.Goal
import com.example.minh.domain.repository.GoalRepository
import com.example.minh.presentation.di.RepositoryModule
import javax.inject.Inject

class SaveToDatabaseUseCase @Inject constructor(
    @RepositoryModule.GoalRepositoryLocal private val goalRepositoryLocal: GoalRepository
) {
    suspend operator fun invoke(goalList: List<Goal>) {
        return goalRepositoryLocal.savaToDatabase(goalList)
    }
}